var class_drink_shop_1_1_logic_1_1_test_1_1_people_test =
[
    [ "SetupTest", "class_drink_shop_1_1_logic_1_1_test_1_1_people_test.html#ad307735a2f00ac08591d1553cb6f0f0e", null ],
    [ "TestDeletePurchase", "class_drink_shop_1_1_logic_1_1_test_1_1_people_test.html#a4083f55d42b7cf6b3f9a2ed79a2cc3b0", null ],
    [ "TestGetOneCustomer", "class_drink_shop_1_1_logic_1_1_test_1_1_people_test.html#afd6443cf884dd8931d85c3679da866bd", null ],
    [ "TestOverAge", "class_drink_shop_1_1_logic_1_1_test_1_1_people_test.html#a360679160b75f93341d9a3de82a47406", null ],
    [ "TestUpdateCustomerAddress", "class_drink_shop_1_1_logic_1_1_test_1_1_people_test.html#a9298ac0a2395699cdf3ffa8f5ae34b09", null ]
];