var class_drink_shop_1_1_web_1_1_controllers_1_1_customers_api_controller =
[
    [ "CustomersApiController", "class_drink_shop_1_1_web_1_1_controllers_1_1_customers_api_controller.html#a600e5e55bc36a946c42d137963e28e8e", null ],
    [ "AddOneCustomer", "class_drink_shop_1_1_web_1_1_controllers_1_1_customers_api_controller.html#a1154ebdddbc54931c0b13a0de8a868a3", null ],
    [ "DelOneCustomer", "class_drink_shop_1_1_web_1_1_controllers_1_1_customers_api_controller.html#aadd9f134fe5bd2806dd53785c9fdbc05", null ],
    [ "GetAll", "class_drink_shop_1_1_web_1_1_controllers_1_1_customers_api_controller.html#a94ee9e6d25a008e6f9ee59cd326572e3", null ],
    [ "ModOneCustomer", "class_drink_shop_1_1_web_1_1_controllers_1_1_customers_api_controller.html#afddc4426f3b11032d122ad514b4ef2bb", null ]
];