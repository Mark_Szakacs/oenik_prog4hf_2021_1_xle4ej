var namespace_drink_shop =
[
    [ "Data", "namespace_drink_shop_1_1_data.html", "namespace_drink_shop_1_1_data" ],
    [ "Logic", "namespace_drink_shop_1_1_logic.html", "namespace_drink_shop_1_1_logic" ],
    [ "Program", "namespace_drink_shop_1_1_program.html", "namespace_drink_shop_1_1_program" ],
    [ "Repository", "namespace_drink_shop_1_1_repository.html", "namespace_drink_shop_1_1_repository" ],
    [ "Web", "namespace_drink_shop_1_1_web.html", "namespace_drink_shop_1_1_web" ],
    [ "Wpf", "namespace_drink_shop_1_1_wpf.html", "namespace_drink_shop_1_1_wpf" ],
    [ "WpfApiClient", "namespace_drink_shop_1_1_wpf_api_client.html", "namespace_drink_shop_1_1_wpf_api_client" ]
];