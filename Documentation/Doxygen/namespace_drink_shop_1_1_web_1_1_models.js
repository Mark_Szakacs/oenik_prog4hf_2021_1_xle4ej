var namespace_drink_shop_1_1_web_1_1_models =
[
    [ "Customer", "class_drink_shop_1_1_web_1_1_models_1_1_customer.html", "class_drink_shop_1_1_web_1_1_models_1_1_customer" ],
    [ "CustomerListViewModel", "class_drink_shop_1_1_web_1_1_models_1_1_customer_list_view_model.html", "class_drink_shop_1_1_web_1_1_models_1_1_customer_list_view_model" ],
    [ "ErrorViewModel", "class_drink_shop_1_1_web_1_1_models_1_1_error_view_model.html", "class_drink_shop_1_1_web_1_1_models_1_1_error_view_model" ],
    [ "MapperFactory", "class_drink_shop_1_1_web_1_1_models_1_1_mapper_factory.html", "class_drink_shop_1_1_web_1_1_models_1_1_mapper_factory" ]
];