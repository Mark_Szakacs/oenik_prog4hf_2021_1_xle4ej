var class_drink_shop_1_1_logic_1_1_over_age =
[
    [ "Equals", "class_drink_shop_1_1_logic_1_1_over_age.html#a1b9932c3c7aee5c1b79e74c800418ae9", null ],
    [ "GetHashCode", "class_drink_shop_1_1_logic_1_1_over_age.html#ac905443f00a00163b58d691ada8ac9a6", null ],
    [ "ToString", "class_drink_shop_1_1_logic_1_1_over_age.html#a607104243cc451a5b0fe44c1aa6bf2c8", null ],
    [ "Age", "class_drink_shop_1_1_logic_1_1_over_age.html#a252fd62806880f9e7aaba8b9a6fc7d7b", null ],
    [ "CustomerName", "class_drink_shop_1_1_logic_1_1_over_age.html#a3e6463075c47189fc338564576b0cfc3", null ],
    [ "PurchaseTime", "class_drink_shop_1_1_logic_1_1_over_age.html#af731ee1e78dcb585b9dcf5e1dbcbb4ad", null ]
];