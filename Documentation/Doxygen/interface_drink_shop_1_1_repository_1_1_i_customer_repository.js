var interface_drink_shop_1_1_repository_1_1_i_customer_repository =
[
    [ "ChangeCustomer", "interface_drink_shop_1_1_repository_1_1_i_customer_repository.html#a6c7a48b39b6e696a251249f98a1e2e86", null ],
    [ "Create", "interface_drink_shop_1_1_repository_1_1_i_customer_repository.html#a88f673e27714a8e6b0eae1cfaa2c64dd", null ],
    [ "Delete", "interface_drink_shop_1_1_repository_1_1_i_customer_repository.html#a478be7d1da5e67865f5094dd1bf68802", null ],
    [ "Update", "interface_drink_shop_1_1_repository_1_1_i_customer_repository.html#a859462c50c3731ee85922d077d6cc833", null ],
    [ "UpdateAddress", "interface_drink_shop_1_1_repository_1_1_i_customer_repository.html#aeb67c5b1c098ac318cae53f39a8974b2", null ],
    [ "UpdateName", "interface_drink_shop_1_1_repository_1_1_i_customer_repository.html#a82ff60fe238fccbd62c9394b8f066424", null ]
];