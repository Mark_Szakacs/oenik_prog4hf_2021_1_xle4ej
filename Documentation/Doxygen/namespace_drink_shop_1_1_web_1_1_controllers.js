var namespace_drink_shop_1_1_web_1_1_controllers =
[
    [ "ApiResult", "class_drink_shop_1_1_web_1_1_controllers_1_1_api_result.html", "class_drink_shop_1_1_web_1_1_controllers_1_1_api_result" ],
    [ "CustomersApiController", "class_drink_shop_1_1_web_1_1_controllers_1_1_customers_api_controller.html", "class_drink_shop_1_1_web_1_1_controllers_1_1_customers_api_controller" ],
    [ "CustomersController", "class_drink_shop_1_1_web_1_1_controllers_1_1_customers_controller.html", "class_drink_shop_1_1_web_1_1_controllers_1_1_customers_controller" ],
    [ "HomeController", "class_drink_shop_1_1_web_1_1_controllers_1_1_home_controller.html", "class_drink_shop_1_1_web_1_1_controllers_1_1_home_controller" ]
];