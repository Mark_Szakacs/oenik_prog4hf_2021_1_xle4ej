var searchData=
[
  ['onconfiguring_394',['OnConfiguring',['../class_drink_shop_1_1_data_1_1_drinks_1_1_drinks_d_b_context.html#a6529cf5d0f8905e7ed809a73e36ca786',1,'DrinkShop::Data::Drinks::DrinksDBContext']]],
  ['onmodelcreating_395',['OnModelCreating',['../class_drink_shop_1_1_data_1_1_drinks_1_1_drinks_d_b_context.html#abfd321bc32ee4d339dab42c330f656db',1,'DrinkShop::Data::Drinks::DrinksDBContext']]],
  ['overage_396',['OverAge',['../class_drink_shop_1_1_logic_1_1_people_logic.html#a2c1ddd1255839ecb60e41a9cb8b22a11',1,'DrinkShop::Logic::PeopleLogic']]],
  ['overageasync_397',['OverAgeAsync',['../interface_drink_shop_1_1_logic_1_1_i_people_logic.html#a9855bc5a242423aeeb964a318f403ddc',1,'DrinkShop.Logic.IPeopleLogic.OverAgeAsync()'],['../class_drink_shop_1_1_logic_1_1_people_logic.html#a09f1f90c5213c2a48b7437c107f0b7af',1,'DrinkShop.Logic.PeopleLogic.OverAgeAsync()']]]
];
