var searchData=
[
  ['brand_440',['Brand',['../class_drink_shop_1_1_data_1_1_drinks_1_1_drinks_t.html#a1fb3398b6d580f84f082be55562f8895',1,'DrinkShop::Data::Drinks::DrinksT']]],
  ['brandid_441',['BrandID',['../class_drink_shop_1_1_data_1_1_drinks_1_1_brand.html#a18290abd2296fa5f87ee12e9f1d6c8cc',1,'DrinkShop.Data.Drinks.Brand.BrandID()'],['../class_drink_shop_1_1_data_1_1_drinks_1_1_drinks_t.html#a58cb8fa87c99747fd84e7936c5b029d1',1,'DrinkShop.Data.Drinks.DrinksT.BrandID()'],['../class_drink_shop_1_1_logic_1_1_average_result.html#ad1121c8f76f9315165a500690640f7f0',1,'DrinkShop.Logic.AverageResult.BrandID()']]],
  ['brandname_442',['BrandName',['../class_drink_shop_1_1_data_1_1_drinks_1_1_brand.html#a5fe06f6be491be87cbc5333e416cd3c4',1,'DrinkShop.Data.Drinks.Brand.BrandName()'],['../class_drink_shop_1_1_logic_1_1_average_result.html#aaad6ab814c9ff11e5dfc925d30eabf93',1,'DrinkShop.Logic.AverageResult.BrandName()'],['../class_drink_shop_1_1_logic_1_1_strong_drink.html#a3b88727de80c97bc2b13dcf4f0e5dd25',1,'DrinkShop.Logic.StrongDrink.BrandName()']]],
  ['brandrep_443',['Brandrep',['../class_drink_shop_1_1_program_1_1_factory.html#a4de95bc84870b21ee22f3157ed3b0b57',1,'DrinkShop::Program::Factory']]],
  ['brands_444',['Brands',['../class_drink_shop_1_1_data_1_1_drinks_1_1_drinks_d_b_context.html#af1135d6f107dcf2e98e1a97d4d35d3e8',1,'DrinkShop::Data::Drinks::DrinksDBContext']]]
];
