var class_drink_shop_1_1_wpf_1_1_b_l_1_1_customer_logic =
[
    [ "CustomerLogic", "class_drink_shop_1_1_wpf_1_1_b_l_1_1_customer_logic.html#a674aad53de9c73a9fd1f12910461f630", null ],
    [ "AddCustomer", "class_drink_shop_1_1_wpf_1_1_b_l_1_1_customer_logic.html#a4239e552919d2eefc6dfc24d86ee9634", null ],
    [ "DelCustomer", "class_drink_shop_1_1_wpf_1_1_b_l_1_1_customer_logic.html#ab6697bf16eb6bc1751ec00498d08402d", null ],
    [ "GetAllCustomer", "class_drink_shop_1_1_wpf_1_1_b_l_1_1_customer_logic.html#a0ad82122f83e85e36dc3da46d58a20c7", null ],
    [ "ModCustomer", "class_drink_shop_1_1_wpf_1_1_b_l_1_1_customer_logic.html#adf34d156aa3d05344b976d88d1130722", null ]
];