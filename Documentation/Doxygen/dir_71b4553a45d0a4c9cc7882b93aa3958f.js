var dir_71b4553a45d0a4c9cc7882b93aa3958f =
[
    [ "DrinkShop", "dir_564e7c5d3ec6f3f137e3a713d5e597a7.html", "dir_564e7c5d3ec6f3f137e3a713d5e597a7" ],
    [ "DrinkShop.Logic", "dir_c3ce7a4e272eb6012540c57c2a8a1533.html", "dir_c3ce7a4e272eb6012540c57c2a8a1533" ],
    [ "DrinkShop.Logic.Test", "dir_cc3b68c15c0e3fe4e28cea0372cb35ed.html", "dir_cc3b68c15c0e3fe4e28cea0372cb35ed" ],
    [ "DrinkShop.Program", "dir_37834a23ca237b6b28779768cb1e5ed6.html", "dir_37834a23ca237b6b28779768cb1e5ed6" ],
    [ "DrinkShop.Repository", "dir_50a8957b663edae4ec8c772dffdeb637.html", "dir_50a8957b663edae4ec8c772dffdeb637" ],
    [ "DrinkShop.Web", "dir_dd334c8f7d59d2a20cad79b971ea71d4.html", "dir_dd334c8f7d59d2a20cad79b971ea71d4" ],
    [ "DrinkShop.Wpf", "dir_06baa7683a7cf0ebc3a6bc411a0b3167.html", "dir_06baa7683a7cf0ebc3a6bc411a0b3167" ],
    [ "Drinkshop.WpfApiClient", "dir_fe6fef19b6beec0799cf2c5cf50d9533.html", "dir_fe6fef19b6beec0799cf2c5cf50d9533" ]
];