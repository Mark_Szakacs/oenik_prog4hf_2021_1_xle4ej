var class_drink_shop_1_1_data_1_1_drinks_1_1_drinks_t =
[
    [ "Equals", "class_drink_shop_1_1_data_1_1_drinks_1_1_drinks_t.html#aa12445289e115d903201c8c20c1f2b62", null ],
    [ "GetHashCode", "class_drink_shop_1_1_data_1_1_drinks_1_1_drinks_t.html#a4b8f92c57a2d9f9420968f3e390177e8", null ],
    [ "ToString", "class_drink_shop_1_1_data_1_1_drinks_1_1_drinks_t.html#a490dec06be384faed095422b07dbbe0d", null ],
    [ "MainData", "class_drink_shop_1_1_data_1_1_drinks_1_1_drinks_t.html#ad5503b30d0737d301adc7265059fcc58", null ],
    [ "Alcpercent", "class_drink_shop_1_1_data_1_1_drinks_1_1_drinks_t.html#a1d3c0cebe76ba59f20047b0c158e60cc", null ],
    [ "Brand", "class_drink_shop_1_1_data_1_1_drinks_1_1_drinks_t.html#a1fb3398b6d580f84f082be55562f8895", null ],
    [ "BrandID", "class_drink_shop_1_1_data_1_1_drinks_1_1_drinks_t.html#a58cb8fa87c99747fd84e7936c5b029d1", null ],
    [ "DrinkID", "class_drink_shop_1_1_data_1_1_drinks_1_1_drinks_t.html#a7727ab93fbbc7d359831fa40c2a57217", null ],
    [ "DrinkName", "class_drink_shop_1_1_data_1_1_drinks_1_1_drinks_t.html#ad34804a4107081600c889e099574bb24", null ],
    [ "Price", "class_drink_shop_1_1_data_1_1_drinks_1_1_drinks_t.html#a87a567778c47d1170d0393173142e2b8", null ],
    [ "Type", "class_drink_shop_1_1_data_1_1_drinks_1_1_drinks_t.html#a6cb44c22875b8ba90e5b052ca6644680", null ]
];