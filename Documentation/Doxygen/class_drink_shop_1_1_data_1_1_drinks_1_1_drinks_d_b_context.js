var class_drink_shop_1_1_data_1_1_drinks_1_1_drinks_d_b_context =
[
    [ "DrinksDBContext", "class_drink_shop_1_1_data_1_1_drinks_1_1_drinks_d_b_context.html#aa5bd0fcf96988e387c5002e6ddf4db45", null ],
    [ "OnConfiguring", "class_drink_shop_1_1_data_1_1_drinks_1_1_drinks_d_b_context.html#a6529cf5d0f8905e7ed809a73e36ca786", null ],
    [ "OnModelCreating", "class_drink_shop_1_1_data_1_1_drinks_1_1_drinks_d_b_context.html#abfd321bc32ee4d339dab42c330f656db", null ],
    [ "Brands", "class_drink_shop_1_1_data_1_1_drinks_1_1_drinks_d_b_context.html#af1135d6f107dcf2e98e1a97d4d35d3e8", null ],
    [ "Customers", "class_drink_shop_1_1_data_1_1_drinks_1_1_drinks_d_b_context.html#a99a8f6cb717cdaacc31a3d8a834003cb", null ],
    [ "Drinks", "class_drink_shop_1_1_data_1_1_drinks_1_1_drinks_d_b_context.html#ad04b9e78578c39963930a9d8234231aa", null ],
    [ "Purchases", "class_drink_shop_1_1_data_1_1_drinks_1_1_drinks_d_b_context.html#a430d4b9f86eff25753043fff69f33964", null ]
];