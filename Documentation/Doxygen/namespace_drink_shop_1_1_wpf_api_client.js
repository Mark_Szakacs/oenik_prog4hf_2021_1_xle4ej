var namespace_drink_shop_1_1_wpf_api_client =
[
    [ "App", "class_drink_shop_1_1_wpf_api_client_1_1_app.html", "class_drink_shop_1_1_wpf_api_client_1_1_app" ],
    [ "CustomerVM", "class_drink_shop_1_1_wpf_api_client_1_1_customer_v_m.html", "class_drink_shop_1_1_wpf_api_client_1_1_customer_v_m" ],
    [ "EditorWindow", "class_drink_shop_1_1_wpf_api_client_1_1_editor_window.html", "class_drink_shop_1_1_wpf_api_client_1_1_editor_window" ],
    [ "IMainLogic", "interface_drink_shop_1_1_wpf_api_client_1_1_i_main_logic.html", "interface_drink_shop_1_1_wpf_api_client_1_1_i_main_logic" ],
    [ "MainLogic", "class_drink_shop_1_1_wpf_api_client_1_1_main_logic.html", "class_drink_shop_1_1_wpf_api_client_1_1_main_logic" ],
    [ "MainVM", "class_drink_shop_1_1_wpf_api_client_1_1_main_v_m.html", "class_drink_shop_1_1_wpf_api_client_1_1_main_v_m" ],
    [ "MainWindow", "class_drink_shop_1_1_wpf_api_client_1_1_main_window.html", "class_drink_shop_1_1_wpf_api_client_1_1_main_window" ],
    [ "MyIoc", "class_drink_shop_1_1_wpf_api_client_1_1_my_ioc.html", "class_drink_shop_1_1_wpf_api_client_1_1_my_ioc" ]
];