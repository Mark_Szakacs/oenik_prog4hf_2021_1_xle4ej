var class_drink_shop_1_1_logic_1_1_people_logic =
[
    [ "PeopleLogic", "class_drink_shop_1_1_logic_1_1_people_logic.html#a7ee2af569e47bfa1074127cee34392a8", null ],
    [ "ChangeCustomer", "class_drink_shop_1_1_logic_1_1_people_logic.html#a0a002026490a2b378e456b4203b4d69c", null ],
    [ "CreateCustomer", "class_drink_shop_1_1_logic_1_1_people_logic.html#a3e9262dab0daf378da3e64279f825b94", null ],
    [ "CreatePurchase", "class_drink_shop_1_1_logic_1_1_people_logic.html#a32238060d513c121e53afc7c17bd1052", null ],
    [ "DeleteCust", "class_drink_shop_1_1_logic_1_1_people_logic.html#a15e3b745cb9403c4199eb6af6e14b2d3", null ],
    [ "DeletePuch", "class_drink_shop_1_1_logic_1_1_people_logic.html#aa63264234b2b2e04ef2b485d317454a9", null ],
    [ "GetAllCustomer", "class_drink_shop_1_1_logic_1_1_people_logic.html#a0a971757c03b57f490d6e41aeb3beb47", null ],
    [ "GetAllPurchase", "class_drink_shop_1_1_logic_1_1_people_logic.html#a456572814b1589b03d0895bb4c5a537b", null ],
    [ "GetCustomerById", "class_drink_shop_1_1_logic_1_1_people_logic.html#a70d7f303cc0111cf3d285bec88551be1", null ],
    [ "GetPurchaseByID", "class_drink_shop_1_1_logic_1_1_people_logic.html#a4a87f5fde629f5c4381832e5aa50b506", null ],
    [ "OverAge", "class_drink_shop_1_1_logic_1_1_people_logic.html#a2c1ddd1255839ecb60e41a9cb8b22a11", null ],
    [ "OverAgeAsync", "class_drink_shop_1_1_logic_1_1_people_logic.html#a09f1f90c5213c2a48b7437c107f0b7af", null ],
    [ "UpdateCustomer", "class_drink_shop_1_1_logic_1_1_people_logic.html#ad840e2493b5bc33405c041f8a6769ad2", null ],
    [ "UpdateCustomerName", "class_drink_shop_1_1_logic_1_1_people_logic.html#a74613e1986b1cafcc350df6cd5c7761d", null ],
    [ "UpdateLocation", "class_drink_shop_1_1_logic_1_1_people_logic.html#ae7fe4282bf3b0a480c5f8d37dc25679b", null ],
    [ "UpdatePurchaseTime", "class_drink_shop_1_1_logic_1_1_people_logic.html#a3f6d6fcb6c47cd1b1beaaa3a3314747d", null ]
];