var interface_drink_shop_1_1_logic_1_1_i_people_logic =
[
    [ "ChangeCustomer", "interface_drink_shop_1_1_logic_1_1_i_people_logic.html#acb695ddf49ffc52f4e014e1160ee66d7", null ],
    [ "CreateCustomer", "interface_drink_shop_1_1_logic_1_1_i_people_logic.html#a2245cf982aeb9f45fdd638bb680ae912", null ],
    [ "CreatePurchase", "interface_drink_shop_1_1_logic_1_1_i_people_logic.html#af30e63c226f7116bb4aae609f9dd3c7e", null ],
    [ "DeleteCust", "interface_drink_shop_1_1_logic_1_1_i_people_logic.html#abb9a91076c2a3754ca3a5018b877a363", null ],
    [ "DeletePuch", "interface_drink_shop_1_1_logic_1_1_i_people_logic.html#a209dececa32692eef1c6eb19e9e17f6f", null ],
    [ "GetAllCustomer", "interface_drink_shop_1_1_logic_1_1_i_people_logic.html#a89216ba9eb245806efd23d598ceb3672", null ],
    [ "GetAllPurchase", "interface_drink_shop_1_1_logic_1_1_i_people_logic.html#a1be6777ad4553be33fbb0ea838e8feeb", null ],
    [ "GetCustomerById", "interface_drink_shop_1_1_logic_1_1_i_people_logic.html#a3578b728f604f677c85aaefc889b1e8f", null ],
    [ "GetPurchaseByID", "interface_drink_shop_1_1_logic_1_1_i_people_logic.html#a92f383e089509443f84396f31a75fed3", null ],
    [ "OverAgeAsync", "interface_drink_shop_1_1_logic_1_1_i_people_logic.html#a9855bc5a242423aeeb964a318f403ddc", null ],
    [ "UpdateCustomer", "interface_drink_shop_1_1_logic_1_1_i_people_logic.html#a7aeb0187482c1ef688cee8d55082c989", null ],
    [ "UpdateCustomerName", "interface_drink_shop_1_1_logic_1_1_i_people_logic.html#a00a2e65ef235672a1c93e36cb467daf9", null ],
    [ "UpdateLocation", "interface_drink_shop_1_1_logic_1_1_i_people_logic.html#a490352e2cfdf14d628f0ac9879986276", null ],
    [ "UpdatePurchaseTime", "interface_drink_shop_1_1_logic_1_1_i_people_logic.html#a28f95e9c494f1452187d6afa7118a28f", null ]
];