var hierarchy =
[
    [ "DrinkShop.Web.Controllers.ApiResult", "class_drink_shop_1_1_web_1_1_controllers_1_1_api_result.html", null ],
    [ "Application", null, [
      [ "DrinkShop.Wpf.App", "class_drink_shop_1_1_wpf_1_1_app.html", null ],
      [ "DrinkShop.WpfApiClient.App", "class_drink_shop_1_1_wpf_api_client_1_1_app.html", null ]
    ] ],
    [ "Application", null, [
      [ "Drinkshop.WpfApiClient.App", "class_drinkshop_1_1_wpf_api_client_1_1_app.html", null ]
    ] ],
    [ "Attribute", null, [
      [ "DrinkShop.Data.Drinks.ToStringAttribute", "class_drink_shop_1_1_data_1_1_drinks_1_1_to_string_attribute.html", null ]
    ] ],
    [ "DrinkShop.Logic.AverageResult", "class_drink_shop_1_1_logic_1_1_average_result.html", null ],
    [ "DrinkShop.Data.Drinks.Brand", "class_drink_shop_1_1_data_1_1_drinks_1_1_brand.html", null ],
    [ "DrinkShop.Repository.BRepository< Brand >", "class_drink_shop_1_1_repository_1_1_b_repository.html", [
      [ "DrinkShop.Repository.BrandRepository", "class_drink_shop_1_1_repository_1_1_brand_repository.html", null ]
    ] ],
    [ "DrinkShop.Repository.BRepository< Customer >", "class_drink_shop_1_1_repository_1_1_b_repository.html", [
      [ "DrinkShop.Repository.CustomerRepository", "class_drink_shop_1_1_repository_1_1_customer_repository.html", null ]
    ] ],
    [ "DrinkShop.Repository.BRepository< DrinksT >", "class_drink_shop_1_1_repository_1_1_b_repository.html", [
      [ "DrinkShop.Repository.DrinkRepository", "class_drink_shop_1_1_repository_1_1_drink_repository.html", null ]
    ] ],
    [ "DrinkShop.Repository.BRepository< Purchase >", "class_drink_shop_1_1_repository_1_1_b_repository.html", [
      [ "DrinkShop.Repository.PurchaseRepository", "class_drink_shop_1_1_repository_1_1_purchase_repository.html", null ]
    ] ],
    [ "Controller", null, [
      [ "DrinkShop.Web.Controllers.CustomersApiController", "class_drink_shop_1_1_web_1_1_controllers_1_1_customers_api_controller.html", null ],
      [ "DrinkShop.Web.Controllers.CustomersController", "class_drink_shop_1_1_web_1_1_controllers_1_1_customers_controller.html", null ],
      [ "DrinkShop.Web.Controllers.HomeController", "class_drink_shop_1_1_web_1_1_controllers_1_1_home_controller.html", null ]
    ] ],
    [ "DrinkShop.Web.Models.Customer", "class_drink_shop_1_1_web_1_1_models_1_1_customer.html", null ],
    [ "DrinkShop.Data.Drinks.Customer", "class_drink_shop_1_1_data_1_1_drinks_1_1_customer.html", null ],
    [ "DrinkShop.Web.Models.CustomerListViewModel", "class_drink_shop_1_1_web_1_1_models_1_1_customer_list_view_model.html", null ],
    [ "DbContext", null, [
      [ "DrinkShop.Data.Drinks.DrinksDBContext", "class_drink_shop_1_1_data_1_1_drinks_1_1_drinks_d_b_context.html", null ]
    ] ],
    [ "DrinkShop.Data.Drinks.DrinksT", "class_drink_shop_1_1_data_1_1_drinks_1_1_drinks_t.html", null ],
    [ "DrinkShop.Web.Models.ErrorViewModel", "class_drink_shop_1_1_web_1_1_models_1_1_error_view_model.html", null ],
    [ "DrinkShop.Program.Factory", "class_drink_shop_1_1_program_1_1_factory.html", null ],
    [ "IComponentConnector", null, [
      [ "DrinkShop.Wpf.UI.EditorWindow", "class_drink_shop_1_1_wpf_1_1_u_i_1_1_editor_window.html", null ],
      [ "Drinkshop.WpfApiClient.MainWindow", "class_drinkshop_1_1_wpf_api_client_1_1_main_window.html", null ]
    ] ],
    [ "DrinkShop.Wpf.BL.ICustomerLogic", "interface_drink_shop_1_1_wpf_1_1_b_l_1_1_i_customer_logic.html", [
      [ "DrinkShop.Wpf.BL.CustomerLogic", "class_drink_shop_1_1_wpf_1_1_b_l_1_1_customer_logic.html", null ]
    ] ],
    [ "DrinkShop.Wpf.BL.IEditorService", "interface_drink_shop_1_1_wpf_1_1_b_l_1_1_i_editor_service.html", [
      [ "DrinkShop.Wpf.UI.EditorServiceViaWindow", "class_drink_shop_1_1_wpf_1_1_u_i_1_1_editor_service_via_window.html", null ]
    ] ],
    [ "DrinkShop.WpfApiClient.IMainLogic", "interface_drink_shop_1_1_wpf_api_client_1_1_i_main_logic.html", [
      [ "DrinkShop.WpfApiClient.MainLogic", "class_drink_shop_1_1_wpf_api_client_1_1_main_logic.html", null ]
    ] ],
    [ "InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "DrinkShop.Logic.IPeopleLogic", "interface_drink_shop_1_1_logic_1_1_i_people_logic.html", [
      [ "DrinkShop.Logic.PeopleLogic", "class_drink_shop_1_1_logic_1_1_people_logic.html", null ]
    ] ],
    [ "DrinkShop.Repository.IRepository< T >", "interface_drink_shop_1_1_repository_1_1_i_repository.html", [
      [ "DrinkShop.Repository.BRepository< T >", "class_drink_shop_1_1_repository_1_1_b_repository.html", null ]
    ] ],
    [ "DrinkShop.Repository.IRepository< Brand >", "interface_drink_shop_1_1_repository_1_1_i_repository.html", [
      [ "DrinkShop.Repository.IBrandRepository", "interface_drink_shop_1_1_repository_1_1_i_brand_repository.html", [
        [ "DrinkShop.Repository.BrandRepository", "class_drink_shop_1_1_repository_1_1_brand_repository.html", null ]
      ] ]
    ] ],
    [ "DrinkShop.Repository.IRepository< Customer >", "interface_drink_shop_1_1_repository_1_1_i_repository.html", [
      [ "DrinkShop.Repository.ICustomerRepository", "interface_drink_shop_1_1_repository_1_1_i_customer_repository.html", [
        [ "DrinkShop.Repository.CustomerRepository", "class_drink_shop_1_1_repository_1_1_customer_repository.html", null ]
      ] ]
    ] ],
    [ "DrinkShop.Repository.IRepository< DrinksT >", "interface_drink_shop_1_1_repository_1_1_i_repository.html", [
      [ "DrinkShop.Repository.IDrinkRepository", "interface_drink_shop_1_1_repository_1_1_i_drink_repository.html", [
        [ "DrinkShop.Repository.DrinkRepository", "class_drink_shop_1_1_repository_1_1_drink_repository.html", null ]
      ] ]
    ] ],
    [ "DrinkShop.Repository.IRepository< Purchase >", "interface_drink_shop_1_1_repository_1_1_i_repository.html", [
      [ "DrinkShop.Repository.IPurchaseRepository", "interface_drink_shop_1_1_repository_1_1_i_purchase_repository.html", [
        [ "DrinkShop.Repository.PurchaseRepository", "class_drink_shop_1_1_repository_1_1_purchase_repository.html", null ]
      ] ]
    ] ],
    [ "DrinkShop.Logic.ISellerLogic", "interface_drink_shop_1_1_logic_1_1_i_seller_logic.html", [
      [ "DrinkShop.Logic.SellerLogic", "class_drink_shop_1_1_logic_1_1_seller_logic.html", null ]
    ] ],
    [ "IServiceLocator", null, [
      [ "DrinkShop.Wpf.MyIoc", "class_drink_shop_1_1_wpf_1_1_my_ioc.html", null ],
      [ "DrinkShop.WpfApiClient.MyIoc", "class_drink_shop_1_1_wpf_api_client_1_1_my_ioc.html", null ]
    ] ],
    [ "DrinkShop.Web.Models.MapperFactory", "class_drink_shop_1_1_web_1_1_models_1_1_mapper_factory.html", null ],
    [ "DrinkShop.Program.Menu", "class_drink_shop_1_1_program_1_1_menu.html", null ],
    [ "DrinkShop.Logic.MostPurchase", "class_drink_shop_1_1_logic_1_1_most_purchase.html", null ],
    [ "ObservableObject", null, [
      [ "DrinkShop.Wpf.Data.CustomerWpf", "class_drink_shop_1_1_wpf_1_1_data_1_1_customer_wpf.html", null ],
      [ "DrinkShop.WpfApiClient.CustomerVM", "class_drink_shop_1_1_wpf_api_client_1_1_customer_v_m.html", null ]
    ] ],
    [ "DrinkShop.Logic.OverAge", "class_drink_shop_1_1_logic_1_1_over_age.html", null ],
    [ "DrinkShop.Logic.Test.PeopleTest", "class_drink_shop_1_1_logic_1_1_test_1_1_people_test.html", null ],
    [ "DrinkShop.Program.Program", "class_drink_shop_1_1_program_1_1_program.html", null ],
    [ "DrinkShop.Web.Program", "class_drink_shop_1_1_web_1_1_program.html", null ],
    [ "DrinkShop.Data.Drinks.Purchase", "class_drink_shop_1_1_data_1_1_drinks_1_1_purchase.html", null ],
    [ "RazorPage", null, [
      [ "AspNetCore.Views__ViewImports", "class_asp_net_core_1_1_views_____view_imports.html", null ],
      [ "AspNetCore.Views__ViewStart", "class_asp_net_core_1_1_views_____view_start.html", null ],
      [ "AspNetCore.Views_Customers_CustomerIndex", "class_asp_net_core_1_1_views___customers___customer_index.html", null ],
      [ "AspNetCore.Views_Customers_CustomersDetails", "class_asp_net_core_1_1_views___customers___customers_details.html", null ],
      [ "AspNetCore.Views_Customers_CustomersEdit", "class_asp_net_core_1_1_views___customers___customers_edit.html", null ],
      [ "AspNetCore.Views_Home_Index", "class_asp_net_core_1_1_views___home___index.html", null ],
      [ "AspNetCore.Views_Home_Privacy", "class_asp_net_core_1_1_views___home___privacy.html", null ],
      [ "AspNetCore.Views_Shared__Layout", "class_asp_net_core_1_1_views___shared_____layout.html", null ],
      [ "AspNetCore.Views_Shared__ValidationScriptsPartial", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial.html", null ],
      [ "AspNetCore.Views_Shared_Error", "class_asp_net_core_1_1_views___shared___error.html", null ]
    ] ],
    [ "RazorPage< IEnumerable< DrinkShop.Web.Models.Customer >>", null, [
      [ "AspNetCore.Views_Customers_CustomerList", "class_asp_net_core_1_1_views___customers___customer_list.html", null ]
    ] ],
    [ "DrinkShop.Logic.Test.SellerTest", "class_drink_shop_1_1_logic_1_1_test_1_1_seller_test.html", null ],
    [ "SimpleIoc", null, [
      [ "DrinkShop.Wpf.MyIoc", "class_drink_shop_1_1_wpf_1_1_my_ioc.html", null ],
      [ "DrinkShop.WpfApiClient.MyIoc", "class_drink_shop_1_1_wpf_api_client_1_1_my_ioc.html", null ]
    ] ],
    [ "DrinkShop.Web.Startup", "class_drink_shop_1_1_web_1_1_startup.html", null ],
    [ "DrinkShop.Logic.StrongDrink", "class_drink_shop_1_1_logic_1_1_strong_drink.html", null ],
    [ "ViewModelBase", null, [
      [ "DrinkShop.Wpf.VM.EditorViewModel", "class_drink_shop_1_1_wpf_1_1_v_m_1_1_editor_view_model.html", null ],
      [ "DrinkShop.Wpf.VM.MainViewModel", "class_drink_shop_1_1_wpf_1_1_v_m_1_1_main_view_model.html", null ],
      [ "DrinkShop.WpfApiClient.MainVM", "class_drink_shop_1_1_wpf_api_client_1_1_main_v_m.html", null ]
    ] ],
    [ "Window", null, [
      [ "DrinkShop.Wpf.MainWindow", "class_drink_shop_1_1_wpf_1_1_main_window.html", null ],
      [ "DrinkShop.Wpf.UI.EditorWindow", "class_drink_shop_1_1_wpf_1_1_u_i_1_1_editor_window.html", null ],
      [ "DrinkShop.WpfApiClient.EditorWindow", "class_drink_shop_1_1_wpf_api_client_1_1_editor_window.html", null ],
      [ "Drinkshop.WpfApiClient.MainWindow", "class_drinkshop_1_1_wpf_api_client_1_1_main_window.html", null ],
      [ "DrinkShop.WpfApiClient.MainWindow", "class_drink_shop_1_1_wpf_api_client_1_1_main_window.html", null ]
    ] ]
];