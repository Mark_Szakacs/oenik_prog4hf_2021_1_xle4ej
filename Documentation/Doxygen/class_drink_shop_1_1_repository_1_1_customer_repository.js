var class_drink_shop_1_1_repository_1_1_customer_repository =
[
    [ "CustomerRepository", "class_drink_shop_1_1_repository_1_1_customer_repository.html#a06be24ec36e51732c167bd9b7d88555b", null ],
    [ "ChangeCustomer", "class_drink_shop_1_1_repository_1_1_customer_repository.html#ae4db758246e35cb7844fefbd649c695a", null ],
    [ "Create", "class_drink_shop_1_1_repository_1_1_customer_repository.html#ab6e343a1ca318ddef1c792caa5a37212", null ],
    [ "Delete", "class_drink_shop_1_1_repository_1_1_customer_repository.html#adce1c0096db8db4f35d033a43719e117", null ],
    [ "GetOne", "class_drink_shop_1_1_repository_1_1_customer_repository.html#a574bf9032d2629689883f047e5fa9099", null ],
    [ "Update", "class_drink_shop_1_1_repository_1_1_customer_repository.html#a45d7e3509a2108319711554594868566", null ],
    [ "UpdateAddress", "class_drink_shop_1_1_repository_1_1_customer_repository.html#a26c20d77d03999c295607b4fbcf3fe3f", null ],
    [ "UpdateName", "class_drink_shop_1_1_repository_1_1_customer_repository.html#a741ad1697f85ef45e20523d44d737982", null ]
];