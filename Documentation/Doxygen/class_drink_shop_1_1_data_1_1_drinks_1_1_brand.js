var class_drink_shop_1_1_data_1_1_drinks_1_1_brand =
[
    [ "Brand", "class_drink_shop_1_1_data_1_1_drinks_1_1_brand.html#a4678062db86bf6bd1a7ebdf6dc633fe0", null ],
    [ "ToString", "class_drink_shop_1_1_data_1_1_drinks_1_1_brand.html#af4e8f8ae25532b79f02d33414cba66c2", null ],
    [ "MainData", "class_drink_shop_1_1_data_1_1_drinks_1_1_brand.html#a27ce6a98a2bc5d3b546d10e43cdd1b05", null ],
    [ "BrandID", "class_drink_shop_1_1_data_1_1_drinks_1_1_brand.html#a18290abd2296fa5f87ee12e9f1d6c8cc", null ],
    [ "BrandName", "class_drink_shop_1_1_data_1_1_drinks_1_1_brand.html#a5fe06f6be491be87cbc5333e416cd3c4", null ],
    [ "Drinks", "class_drink_shop_1_1_data_1_1_drinks_1_1_brand.html#a4faea57ca19d5739b941b7f65031809e", null ]
];