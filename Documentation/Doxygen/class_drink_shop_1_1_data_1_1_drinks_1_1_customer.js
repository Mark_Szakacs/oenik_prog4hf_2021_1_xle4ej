var class_drink_shop_1_1_data_1_1_drinks_1_1_customer =
[
    [ "ToString", "class_drink_shop_1_1_data_1_1_drinks_1_1_customer.html#ad904009386c1908007f0a8cf3ed2f306", null ],
    [ "MainData", "class_drink_shop_1_1_data_1_1_drinks_1_1_customer.html#af95668c76a27480d99ded57f5073d77e", null ],
    [ "Age", "class_drink_shop_1_1_data_1_1_drinks_1_1_customer.html#a8ee5a8fec57f3e54771868a44d5bf1b9", null ],
    [ "CostumerAddress", "class_drink_shop_1_1_data_1_1_drinks_1_1_customer.html#a104b0d7a9b901f8b7c97b8ef2913c2b2", null ],
    [ "CostumerID", "class_drink_shop_1_1_data_1_1_drinks_1_1_customer.html#ad89456956a56bdf285c91701aa8ccf9f", null ],
    [ "CostumerName", "class_drink_shop_1_1_data_1_1_drinks_1_1_customer.html#ad840cfee8fa0b6e472735019cbf2e226", null ],
    [ "Phone", "class_drink_shop_1_1_data_1_1_drinks_1_1_customer.html#a80f86b7d99b50992f31fcc8383b70e67", null ],
    [ "Purchase", "class_drink_shop_1_1_data_1_1_drinks_1_1_customer.html#a25a82c348c2e103274236f8676a784f8", null ],
    [ "PurchaseID", "class_drink_shop_1_1_data_1_1_drinks_1_1_customer.html#af9f62288fc5670917571e3b3e32b8248", null ]
];