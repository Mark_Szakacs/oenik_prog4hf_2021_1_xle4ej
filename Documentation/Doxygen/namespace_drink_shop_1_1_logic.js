var namespace_drink_shop_1_1_logic =
[
    [ "Test", "namespace_drink_shop_1_1_logic_1_1_test.html", "namespace_drink_shop_1_1_logic_1_1_test" ],
    [ "AverageResult", "class_drink_shop_1_1_logic_1_1_average_result.html", "class_drink_shop_1_1_logic_1_1_average_result" ],
    [ "IPeopleLogic", "interface_drink_shop_1_1_logic_1_1_i_people_logic.html", "interface_drink_shop_1_1_logic_1_1_i_people_logic" ],
    [ "ISellerLogic", "interface_drink_shop_1_1_logic_1_1_i_seller_logic.html", "interface_drink_shop_1_1_logic_1_1_i_seller_logic" ],
    [ "MostPurchase", "class_drink_shop_1_1_logic_1_1_most_purchase.html", "class_drink_shop_1_1_logic_1_1_most_purchase" ],
    [ "OverAge", "class_drink_shop_1_1_logic_1_1_over_age.html", "class_drink_shop_1_1_logic_1_1_over_age" ],
    [ "PeopleLogic", "class_drink_shop_1_1_logic_1_1_people_logic.html", "class_drink_shop_1_1_logic_1_1_people_logic" ],
    [ "SellerLogic", "class_drink_shop_1_1_logic_1_1_seller_logic.html", "class_drink_shop_1_1_logic_1_1_seller_logic" ],
    [ "StrongDrink", "class_drink_shop_1_1_logic_1_1_strong_drink.html", "class_drink_shop_1_1_logic_1_1_strong_drink" ]
];