var class_drink_shop_1_1_wpf_1_1_data_1_1_customer_wpf =
[
    [ "CopyFrom", "class_drink_shop_1_1_wpf_1_1_data_1_1_customer_wpf.html#a7f9135316f8106e0b598ff6bae0cf4c0", null ],
    [ "CreateEntity", "class_drink_shop_1_1_wpf_1_1_data_1_1_customer_wpf.html#a31f43fa4a698f6fb8886a6ab204e29ac", null ],
    [ "CreateModelFromEntity", "class_drink_shop_1_1_wpf_1_1_data_1_1_customer_wpf.html#af9c098baa0ba4cbc58e3cbb54383f9af", null ],
    [ "Age", "class_drink_shop_1_1_wpf_1_1_data_1_1_customer_wpf.html#a9511f7325683162ce0edb8a58c9684a8", null ],
    [ "CustomerAddress", "class_drink_shop_1_1_wpf_1_1_data_1_1_customer_wpf.html#a3b579d23e2324eadebdea0e8a06ad76f", null ],
    [ "CustomerID", "class_drink_shop_1_1_wpf_1_1_data_1_1_customer_wpf.html#a2f8e1d875a664f2e90316157e0cfa5ec", null ],
    [ "CustomerName", "class_drink_shop_1_1_wpf_1_1_data_1_1_customer_wpf.html#ae4d106377275c5b10dd6340d33091aab", null ],
    [ "Phone", "class_drink_shop_1_1_wpf_1_1_data_1_1_customer_wpf.html#a525c0d7793777d666a429f591cef4704", null ]
];