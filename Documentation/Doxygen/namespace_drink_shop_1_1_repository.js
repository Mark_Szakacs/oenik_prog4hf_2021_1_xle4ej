var namespace_drink_shop_1_1_repository =
[
    [ "BrandRepository", "class_drink_shop_1_1_repository_1_1_brand_repository.html", "class_drink_shop_1_1_repository_1_1_brand_repository" ],
    [ "BRepository", "class_drink_shop_1_1_repository_1_1_b_repository.html", "class_drink_shop_1_1_repository_1_1_b_repository" ],
    [ "CustomerRepository", "class_drink_shop_1_1_repository_1_1_customer_repository.html", "class_drink_shop_1_1_repository_1_1_customer_repository" ],
    [ "DrinkRepository", "class_drink_shop_1_1_repository_1_1_drink_repository.html", "class_drink_shop_1_1_repository_1_1_drink_repository" ],
    [ "IBrandRepository", "interface_drink_shop_1_1_repository_1_1_i_brand_repository.html", "interface_drink_shop_1_1_repository_1_1_i_brand_repository" ],
    [ "ICustomerRepository", "interface_drink_shop_1_1_repository_1_1_i_customer_repository.html", "interface_drink_shop_1_1_repository_1_1_i_customer_repository" ],
    [ "IDrinkRepository", "interface_drink_shop_1_1_repository_1_1_i_drink_repository.html", "interface_drink_shop_1_1_repository_1_1_i_drink_repository" ],
    [ "IPurchaseRepository", "interface_drink_shop_1_1_repository_1_1_i_purchase_repository.html", "interface_drink_shop_1_1_repository_1_1_i_purchase_repository" ],
    [ "IRepository", "interface_drink_shop_1_1_repository_1_1_i_repository.html", "interface_drink_shop_1_1_repository_1_1_i_repository" ],
    [ "PurchaseRepository", "class_drink_shop_1_1_repository_1_1_purchase_repository.html", "class_drink_shop_1_1_repository_1_1_purchase_repository" ]
];