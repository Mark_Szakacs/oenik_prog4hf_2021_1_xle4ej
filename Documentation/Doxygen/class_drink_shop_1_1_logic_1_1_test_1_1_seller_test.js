var class_drink_shop_1_1_logic_1_1_test_1_1_seller_test =
[
    [ "SetupTest", "class_drink_shop_1_1_logic_1_1_test_1_1_seller_test.html#add184ddb753fccbaf4949c309c65b875", null ],
    [ "TestBrandAvragesResult", "class_drink_shop_1_1_logic_1_1_test_1_1_seller_test.html#a2efc127ece6c5a19af8147df0e1bb652", null ],
    [ "TestCreateBrand", "class_drink_shop_1_1_logic_1_1_test_1_1_seller_test.html#ac975e47d1026ac908842beb9f92c63c3", null ],
    [ "TestDrinkGetAll", "class_drink_shop_1_1_logic_1_1_test_1_1_seller_test.html#a3d8706a4944353b2387d07183dd27f05", null ],
    [ "TestMostExpensiveDrink", "class_drink_shop_1_1_logic_1_1_test_1_1_seller_test.html#a94fa317d8aff31ac61c1ac940609aa29", null ],
    [ "TestStrongDrinks", "class_drink_shop_1_1_logic_1_1_test_1_1_seller_test.html#abe93c9c3b00d9af6e7675a102261e72c", null ]
];