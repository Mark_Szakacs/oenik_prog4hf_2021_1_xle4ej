var class_drink_shop_1_1_logic_1_1_seller_logic =
[
    [ "SellerLogic", "class_drink_shop_1_1_logic_1_1_seller_logic.html#ab603e858b0bcf0e13f0ec5de4ff3521d", null ],
    [ "SellerLogic", "class_drink_shop_1_1_logic_1_1_seller_logic.html#a39fb62ef8be61532a06ade7f7cf0f6cd", null ],
    [ "SellerLogic", "class_drink_shop_1_1_logic_1_1_seller_logic.html#ac40df699d485ee0ad4f0fc92931146a6", null ],
    [ "CreateBrand", "class_drink_shop_1_1_logic_1_1_seller_logic.html#a1530590f97ad40802b42c1e05acad33d", null ],
    [ "CreateDrink", "class_drink_shop_1_1_logic_1_1_seller_logic.html#a69484406a458696bb81f5ae0f8a0ea21", null ],
    [ "DeleteBrand", "class_drink_shop_1_1_logic_1_1_seller_logic.html#ab475a6c0de50749592fedab126874a65", null ],
    [ "DeleteDrink", "class_drink_shop_1_1_logic_1_1_seller_logic.html#a380bf94490c76c229e142017f39a8be6", null ],
    [ "GetAllBrand", "class_drink_shop_1_1_logic_1_1_seller_logic.html#a27c10d822902d1cc9e3e4e6a0d8e6986", null ],
    [ "GetAllDrinks", "class_drink_shop_1_1_logic_1_1_seller_logic.html#ad7bf4dd372ad24b62f7ac111f36f0f99", null ],
    [ "GetBrandById", "class_drink_shop_1_1_logic_1_1_seller_logic.html#a45f551d2e11edb19736472a0b53bcd0e", null ],
    [ "GetDrinkById", "class_drink_shop_1_1_logic_1_1_seller_logic.html#af016fd68538435ea48d11ff5361fdf9c", null ],
    [ "GetDrinksbyBrand", "class_drink_shop_1_1_logic_1_1_seller_logic.html#ad41864542e39d1bea0ece09c9633189c", null ],
    [ "GetPriveAvrage", "class_drink_shop_1_1_logic_1_1_seller_logic.html#aa6de485a638c982a3a9833135973e7cb", null ],
    [ "GetPriveAvrageAsync", "class_drink_shop_1_1_logic_1_1_seller_logic.html#affaca9a182ec67cc313e549cdd824dab", null ],
    [ "MostExpensiveDrink", "class_drink_shop_1_1_logic_1_1_seller_logic.html#aa3f20e88b5e226edae5a22f8ca1f372e", null ],
    [ "MostExpensiveDrinkAsync", "class_drink_shop_1_1_logic_1_1_seller_logic.html#a19eec0496f76844edeeb856574a82b3e", null ],
    [ "StrongDrinks", "class_drink_shop_1_1_logic_1_1_seller_logic.html#a87970d12caff94d0a0b1ac619b8e4701", null ],
    [ "StrongDrinksAsync", "class_drink_shop_1_1_logic_1_1_seller_logic.html#a9fbaedf0c0e4f9b166ededa30b6bf698", null ],
    [ "UpdateBrandName", "class_drink_shop_1_1_logic_1_1_seller_logic.html#a350546bcdf51ee266129a299b235e6e5", null ],
    [ "UpdateDrinkName", "class_drink_shop_1_1_logic_1_1_seller_logic.html#a4ab20c7342a4075f4142db2717db7c6f", null ],
    [ "UpdateDrinkPrice", "class_drink_shop_1_1_logic_1_1_seller_logic.html#a1a55e6e807859222661d0e698608c558", null ]
];