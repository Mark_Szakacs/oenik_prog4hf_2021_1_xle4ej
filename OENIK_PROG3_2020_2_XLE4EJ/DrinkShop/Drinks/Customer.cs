﻿// <copyright file="Customer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrinkShop.Data.Drinks
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Reflection;
    using System.Text;

    /// <summary>
    /// Customer table.
    /// </summary>
    [Table("Customer")]
    public class Customer
    {
        /// <summary>
        /// Gets or sets of customerID.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [ToString]
        public int? CostumerID { get; set; }

        /// <summary>
        /// Gets or sets of customerName.
        /// </summary>
        [MaxLength(100)]
        [ToString]
        public string CostumerName { get; set; }

        /// <summary>
        /// Gets or sets of age.
        /// </summary>
        [ToString]
        public int Age { get; set; }

        /// <summary>
        /// Gets or sets of phone.
        /// </summary>
        [MaxLength(100)]
        [ToString]
        public string Phone { get; set; }

        /// <summary>
        /// Gets or sets of address.
        /// </summary>
        [MaxLength(100)]
        [ToString]
        public string CostumerAddress { get; set; }

        /// <summary>
        /// Gets or sets of purchaseID.
        /// </summary>
        [ForeignKey(nameof(Purchase))]
        public int? PurchaseID { get; set; }

        /// <summary>
        /// Gets or sets of forein purchase.
        /// </summary>
        [NotMapped]
        public virtual Purchase Purchase { get; set; }

        /// <summary>
        /// Gets the main data.
        /// </summary>
        [NotMapped]
        public string MainData => $"[{this.CostumerID}] : {this.CostumerName} : {this.Age} : {this.Phone} : {this.CostumerAddress}";

        /// <summary>
        /// Tostring to write the datas.
        /// </summary>
        /// <returns>with x.</returns>
        public override string ToString()
        {
            string x = " ";

            foreach (var item in this.GetType().GetProperties().Where(x => x.GetCustomAttribute<ToStringAttribute>() != null))
            {
                x = "  ";
                x += item.Name + "\t =>";
                x += item.GetValue(this);
                x += "\n";
            }

            return x;
        }
    }
}
