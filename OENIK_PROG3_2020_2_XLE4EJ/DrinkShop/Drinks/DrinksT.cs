﻿// <copyright file="DrinksT.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrinkShop.Data.Drinks
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using Microsoft.EntityFrameworkCore.Query.SqlExpressions;

    /// <summary>
    /// Drinks Table.
    /// </summary>
    [Table("Drinks")]
    public class DrinksT
    {
        /// <summary>
        /// Gets or sets of DrinkID.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [ToString]
        public int DrinkID { get; set; }

        /// <summary>
        /// Gets or sets of DrinkName.
        /// </summary>
        [MaxLength(100)]
        [Required]
        [ToString]
        public string DrinkName { get; set; }

        /// <summary>
        /// Gets or sets of Type.
        /// </summary>
        [MaxLength(100)]
        [Required]
        [ToString]
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets of BrandID.
        /// </summary>
        [ForeignKey(nameof(Brand))]
        public int BrandID { get; set; }

        /// <summary>
        /// Gets or sets of Brand.
        /// </summary>
        [NotMapped]
        public virtual Brand Brand { get; set; }

        /// <summary>
        /// Gets or sets of Alcpercent.
        /// </summary>
        [MaxLength(100)]
        [Required]
        [ToString]
        public double? Alcpercent { get; set; }

        /// <summary>
        /// Gets or sets of Price.
        /// </summary>
        [MaxLength(100)]
        [Required]
        [ToString]
        public int? Price { get; set; }

        /// <summary>
        /// Gets or set of maindata.
        /// </summary>
        [NotMapped]
        public string MainData => $"[{this.DrinkID}] : {this.DrinkName} : Type : {this.Type} : {this.Brand} : Alc: {this.Alcpercent}% : Price :{this.Price} ";

        /// <summary>
        /// Tosrtring for datas.
        /// </summary>
        /// <returns>with x.</returns>
        public override string ToString()
        {
            string x = "  ";

            foreach (var item in this.GetType().GetProperties().Where(x => x.GetCustomAttribute<ToStringAttribute>() != null))
            {
                x = "   ";
                x += item.Name + " :";
                x += item.GetValue(this);
                x += " ";
            }

            return x;
        }

        /// <summary>
        /// Equals.
        /// </summary>
        /// <param name="obj">oject.</param>
        /// <returns>with obj.</returns>
        public override bool Equals(object obj)
        {
            if (obj is DrinksT)
            {
                DrinksT other = obj as DrinksT;
                return this.DrinkID == other.DrinkID &&
                    this.DrinkName == other.DrinkName &&
                    this.Brand == other.Brand &&
                    this.Type == other.Type &&
                    this.Price == other.Price &&
                    this.Alcpercent == other.Alcpercent;
            }

            return false;
        }

        /// <summary>
        /// Gethashcode.
        /// </summary>
        /// <returns>Drink ID.</returns>
        public override int GetHashCode()
        {
            return this.DrinkID;
        }
    }
}
