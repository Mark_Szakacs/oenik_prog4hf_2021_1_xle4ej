﻿// <copyright file="Purchase.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrinkShop.Data.Drinks
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text;

    /// <summary>
    /// Purchase class.
    /// </summary>
    [Table("Purchases")]
    public class Purchase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Purchase"/> class.
        /// </summary>
        public Purchase()
        {
            this.Customers = new HashSet<Customer>();
        }

        /// <summary>
        /// Gets or sets the primary key of the brand table.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PurchaseID { get; set; }

        /// <summary>
        /// Gets or sets of PurchaseDate .
        /// </summary>
        [Required]
        public DateTime PurchaseDate { get; set; }

        /// <summary>
        /// Gets or sets of CustomerID.
        /// </summary>
        [ForeignKey(nameof(Customer))]
        public string CustomerName { get; set; }

        /// <summary>
        /// Gets or sets of Customer foreign.
        /// </summary>
        [NotMapped]
        public virtual Customer Customer { get; set; }

        /// <summary>
        /// Gets or sets os sets Icollection of customers.
        /// </summary>
        public virtual ICollection<Customer> Customers { get; set; }
    }
}
