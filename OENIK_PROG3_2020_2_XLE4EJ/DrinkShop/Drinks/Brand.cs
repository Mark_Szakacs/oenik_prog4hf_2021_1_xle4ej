﻿// <copyright file="Brand.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrinkShop.Data.Drinks
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Reflection;
    using System.Text;

    /// <summary>
    /// Brand Class.
    /// </summary>
    [Table("Brands")]
    public class Brand
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Brand"/> class.
        /// </summary>
        public Brand()
        {
            this.Drinks = new HashSet<DrinksT>();
        }

        /// <summary>
        /// Gets or sets the primary key of the brand table.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [ToString]
        public int BrandID { get; set; }

        /// <summary>
        /// Gets or sets the names of brands.
        /// </summary>
        [MaxLength(100)]
        [Required]
        [ToString]
        public string BrandName { get; set; }

        /// <summary>
        /// Gets or sets of Icollection.
        /// </summary>
        public virtual ICollection<DrinksT> Drinks { get; set; }

        /// <summary>
        /// Gets Main data.
        /// </summary>
        [NotMapped]
        public string MainData => $"[{this.BrandID}] : {this.BrandName} ";

        /// <summary>
        /// Tostring method to write the datas.
        /// </summary>
        /// <returns>return with x.</returns>
        public override string ToString()
        {
            string x = "  ";

            foreach (var item in this.GetType().GetProperties().Where(x => x.GetCustomAttribute<ToStringAttribute>() != null))
            {
                x = "   ";
                x += item.Name + "\t =>";
                x += item.GetValue(this);
                x += "\n";
            }

            return x;
        }
    }
}
