﻿// <copyright file="DrinksDBContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrinkShop.Data.Drinks
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Options;

    /// <summary>
    /// DrinksDBContext class.
    /// </summary>
    public partial class DrinksDBContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DrinksDBContext"/> class.
        /// Create the tables.
        /// </summary>
        public DrinksDBContext()
        {
            this.Database.EnsureCreated();
        }

        /// <summary>
        /// Gets or sets for Brands in DrinksDBContext.
        /// </summary>
        public virtual DbSet<Brand> Brands { get; set; }

        /// <summary>
        /// Gets or sets for Drinks in DrinksDBContext.
        /// </summary>
        public virtual DbSet<DrinksT> Drinks { get; set; }

        /// <summary>
        /// Gets or sets for Costumer in DrinksDBContext.
        /// </summary>
        public virtual DbSet<Customer> Customers { get; set; }

        /// <summary>
        /// Gets or sets for Purchases in DrinksDBContext.
        /// </summary>
        public virtual DbSet<Purchase> Purchases { get; set; }

        /// <summary>
        /// OnCunfiguring(lazy load).
        /// </summary>
        /// <param name="optionsBuilder">optionsbuilder.</param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseLazyLoadingProxies().UseSqlServer(@"Data Source=(LocalDB)\MSSQLLocalDB; AttachDbFilename=|DataDirectory|\DrinkShop2.mdf; Integrated Security=True; MultipleActiveResultSets=True");
            }
        }

        /// <summary>
        /// OnModelCreating to fill the database.
        /// </summary>
        /// <param name="modelBuilder"> context.</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Brand>()
            .HasMany(brands => brands.Drinks)
            .WithOne(drinks => drinks.Brand)
            .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<DrinksT>()
            .HasOne(drinks => drinks.Brand)
            .WithMany(brands => brands.Drinks)
            .HasForeignKey(drinks => drinks.BrandID)
            .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Customer>()
                .HasOne(c => c.Purchase)
                .WithMany(a => a.Customers)
                .HasForeignKey(c => c.PurchaseID)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Purchase>()
                .HasMany(a => a.Customers)
                .WithOne(b => b.Purchase)
                .OnDelete(DeleteBehavior.Cascade);

            DrinksT d1 = new DrinksT() { DrinkID = 1, DrinkName = "Hendricks", Type = "Gin", BrandID = 1, Alcpercent = 41, Price = 9990 };
            DrinksT d2 = new DrinksT() { DrinkID = 2, DrinkName = "Hendricks Midsummer", Type = "Gin", BrandID = 1, Alcpercent = 44.3, Price = 14990 };
            DrinksT d3 = new DrinksT() { DrinkID = 3, DrinkName = "Öreg", Type = "Gin", BrandID = 2, Alcpercent = 40, Price = 19990 };
            DrinksT d4 = new DrinksT() { DrinkID = 4, DrinkName = "Monkey 47", Type = "Gin", BrandID = 3, Alcpercent = 47, Price = 13990 };
            DrinksT d5 = new DrinksT() { DrinkID = 5, DrinkName = "Jameson", Type = "Whiskey", BrandID = 4, Alcpercent = 40, Price = 9990 };
            DrinksT d6 = new DrinksT() { DrinkID = 6, DrinkName = "Jack Daniels", Type = "Whiskey", BrandID = 5, Alcpercent = 45, Price = 10990 };
            DrinksT d7 = new DrinksT() { DrinkID = 7, DrinkName = "Jameson Black", Type = "Whiskey", BrandID = 4, Alcpercent = 43.3, Price = 13990 };
            DrinksT d8 = new DrinksT() { DrinkID = 8, DrinkName = "Monkey 47 Orange", Type = "Gin", BrandID = 3, Alcpercent = 22, Price = 9990 };
            DrinksT d9 = new DrinksT() { DrinkID = 9, DrinkName = "Gentlemans Jack", Type = "Whiskey", BrandID = 5, Alcpercent = 40.2, Price = 9990 };
            DrinksT d10 = new DrinksT() { DrinkID = 10, DrinkName = "Jack deniels Fire", Type = "Whiskey", BrandID = 5, Alcpercent = 43, Price = 13000 };
            DrinksT d11 = new DrinksT() { DrinkID = 11, DrinkName = "Talisker Wild", Type = "Whiskey", BrandID = 6, Alcpercent = 31, Price = 15000 };

            Brand b1 = new Brand() { BrandID = 1, BrandName = "Hendricks" };
            Brand b2 = new Brand() { BrandID = 2, BrandName = "Öreg" };
            Brand b3 = new Brand() { BrandID = 3, BrandName = "Monkey 47" };
            Brand b4 = new Brand() { BrandID = 4, BrandName = "Jameson" };
            Brand b5 = new Brand() { BrandID = 5, BrandName = "Jack Daniels" };
            Brand b6 = new Brand() { BrandID = 6, BrandName = "Talisker" };

            Customer c1 = new Customer() { CostumerID = 1, CostumerName = "Eros Pista", Age = 19, Phone = "06301953097", CostumerAddress = "Árpád fejedelem útja 57", PurchaseID = 1 };
            Customer c2 = new Customer() { CostumerID = 2, CostumerName = "Mara A Müllner", Age = 39, Phone = "06301953097", CostumerAddress = "Nyár utca 16", PurchaseID = 1 };
            Customer c3 = new Customer() { CostumerID = 3, CostumerName = "Kiss Lajos", Age = 66, Phone = "06301953097", CostumerAddress = "Kossuth Lajos u. 53", PurchaseID = 2 };
            Customer c4 = new Customer() { CostumerID = 4, CostumerName = "Pintér Laura", Age = 15, Phone = "06301953097", CostumerAddress = "Csavargyár u. 25", PurchaseID = 3 };

            Purchase p1 = new Purchase() { PurchaseID = 1, CustomerName = " ",  PurchaseDate = new DateTime(2008, 5, 14, 8, 30, 32), };
            Purchase p2 = new Purchase() { PurchaseID = 2, CustomerName = " ", PurchaseDate = new DateTime(1999, 12, 11, 9, 30, 52), };
            Purchase p3 = new Purchase() { PurchaseID = 3, CustomerName = " ",  PurchaseDate = new DateTime(1994, 3, 23, 15, 30, 32), };
            Purchase p4 = new Purchase() { PurchaseID = 4, CustomerName = " ", PurchaseDate = new DateTime(1970, 7, 29, 18, 30, 42), };
            Purchase p5 = new Purchase() { PurchaseID = 5, CustomerName = " ", PurchaseDate = new DateTime(2000, 6, 1, 7, 30, 2), };

            d1.BrandID = b1.BrandID;
            d2.BrandID = b1.BrandID;

            d3.BrandID = b2.BrandID;

            d4.BrandID = b3.BrandID;

            d5.BrandID = b4.BrandID;
            d6.BrandID = b5.BrandID;
            p1.CustomerName = c3.CostumerName;

            p2.CustomerName = c1.CostumerName;

            p3.CustomerName = c1.CostumerName;

            p4.CustomerName = c4.CostumerName;

            p4.CustomerName = c2.CostumerName;

            modelBuilder.Entity<Brand>().HasData(b1, b2, b3, b4, b5, b6);
            modelBuilder.Entity<DrinksT>().HasData(d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, d11);
            modelBuilder.Entity<Customer>().HasData(c1, c2, c3, c4);
            modelBuilder.Entity<Purchase>().HasData(p1, p2, p3, p4, p5);
        }
    }
}
