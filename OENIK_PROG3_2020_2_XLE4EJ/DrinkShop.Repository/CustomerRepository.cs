﻿// <copyright file="CustomerRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrinkShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DrinkShop.Data.Drinks;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// ejnkvdsjkvds.
    /// </summary>
    public class CustomerRepository : BRepository<Customer>, ICustomerRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerRepository"/> class.
        /// </summary>
        /// <param name="ctx">asdwed.</param>
        public CustomerRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// Create a customer.
        /// </summary>
        /// <param name="customer">A new customer to add.</param>
        public void Create(Customer customer)
        {
            this.ctx.Add(customer);
            this.ctx.SaveChanges();
        }

        /// <summary>
        /// Deleting a customer.
        /// </summary>
        /// <param name="id">The id of the customer.</param>
        public void Delete(int id)
        {
            this.ctx.Remove(this.GetOne(id));
            this.ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public override Customer GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.CostumerID == id);
        }

        /// <summary>
        /// Update the address of a customer.
        /// </summary>
        /// <param name="id">The id of the customer.</param>
        /// <param name="address">The new address of the customer.</param>
        public void UpdateAddress(int id, string address)
        {
            var customer = this.GetOne(id);
            customer.CostumerAddress = address;
            this.ctx.SaveChanges();
        }

        /// <summary>
        /// Update a name of a customer.
        /// </summary>
        /// <param name="id">tThe id of the customer.</param>
        /// <param name="name">The new name of the customer.</param>
        public void UpdateName(int id, string name)
        {
            var customer = this.GetOne(id);
            customer.CostumerName = name;
            this.ctx.SaveChanges();
        }

        /// <summary>
        /// Update all of Customers porp.
        /// </summary>
        /// <param name="customer">entity.</param>
        public void Update(Customer customer)
        {
            this.ctx.Set<Customer>().Update(customer);
            this.ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public bool ChangeCustomer(int id, string name, string address, string phone, int age)
        {
            var customer = this.GetOne(id);
            if (customer == null)
            {
                return false;
            }
            else
            {
                customer.CostumerName = name;
                customer.CostumerAddress = address;
                customer.Age = age;
                customer.Phone = phone;
                this.ctx.SaveChanges();
                return true;
            }
        }
    }
}
