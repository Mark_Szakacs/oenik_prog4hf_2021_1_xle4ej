﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrinkShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Collective interface of the repo classes.
    /// </summary>
    /// <typeparam name="T">Generic parameter.</typeparam>
    public interface IRepository<T>
        where T : class
    {
        /// <summary>
        /// Returns one entity of the given type from the database.
        /// </summary>
        /// <param name="id">The id of the chosen entity.</param>
        /// <returns>The chosen entity.</returns>
        T GetOne(int id);

        /// <summary>
        /// Returns every entities of the given type from the database.
        /// </summary>
        /// <returns>List of chosen entities.</returns>
        IQueryable<T> GetAll();

        /// <summary>
        /// Insert a new entity.
        /// </summary>
        /// <param name="entity">new entity.</param>
        public void Insert(T entity);

        /// <summary>
        /// Remove an entity.
        /// </summary>
        /// <param name="id">the id of the entity.</param>
        /// <returns>with the entity we want to remove.</returns>
        public bool Remove(int id);
    }
}
