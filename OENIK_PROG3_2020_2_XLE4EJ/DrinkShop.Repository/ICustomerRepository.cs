﻿// <copyright file="ICustomerRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrinkShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using DrinkShop.Data.Drinks;

    /// <summary>
    /// Icustomer Interface.
    /// </summary>
    public interface ICustomerRepository : IRepository<Customer>
    {
        /// <summary>
        /// Create a customer.
        /// </summary>
        /// <param name="customer">A new customer to add.</param>
        void Create(Customer customer);

        /// <summary>
        /// Update a name of a customer.
        /// </summary>
        /// <param name="id">tThe id of the customer.</param>
        /// <param name="name">The new name of the customer.</param>
        void UpdateName(int id, string name);

        /// <summary>
        /// Update the address of a customer.
        /// </summary>
        /// <param name="id">The id of the customer.</param>
        /// <param name="address">The new address of the customer.</param>
        void UpdateAddress(int id, string address);

        /// <summary>
        /// Deleting a customer.
        /// </summary>
        /// <param name="id">The id of the customer.</param>
        void Delete(int id);

        /// <summary>
        /// All Update.
        /// </summary>
        /// <param name="customer">entity.</param>
        void Update(Customer customer);

        /// <summary>
        /// Change Customer.
        /// </summary>
        /// <param name="id">id of customer.</param>
        /// <param name="name">name of customer.</param>
        /// <param name="address">address of customer.</param>
        /// <param name="phone">phone of customer.</param>
        /// <param name="age">age of customer.</param>
        /// <returns>true if can change.</returns>
        public bool ChangeCustomer(int id, string name, string address, string phone, int age);
    }
}
