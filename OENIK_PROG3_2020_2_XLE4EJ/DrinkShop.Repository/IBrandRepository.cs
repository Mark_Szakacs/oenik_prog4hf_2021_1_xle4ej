﻿// <copyright file="IBrandRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrinkShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using DrinkShop.Data.Drinks;

    /// <summary>
    /// Ibrandrepo interface.
    /// </summary>
    public interface IBrandRepository : IRepository<Brand>
    {
        /// <summary>
        /// Create a brand.
        /// </summary>
        /// <param name="brand">A new brand to add.</param>
        void Create(Brand brand);

        /// <summary>
        /// Update the name of a brand.
        /// </summary>
        /// <param name="id">The id of a brand.</param>
        /// <param name="name">The new id of a brand.</param>
        void UpdateName(int id, string name);

        /// <summary>
        /// Deleting a brand.
        /// </summary>
        /// <param name="id">The id of the brand.</param>
        void Delete(int id);
    }
}
