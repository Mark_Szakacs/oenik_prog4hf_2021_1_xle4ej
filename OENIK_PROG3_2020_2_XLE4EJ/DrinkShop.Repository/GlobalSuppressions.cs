﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1401:Fields should be private", Justification = "<Pending>", Scope = "member", Target = "~F:DrinkShop.Repository.BRepository`1.ctx")]
[assembly: SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1404:Code analysis suppression should have justification", Justification = "<Pending>")]
[assembly: SuppressMessage("Naming", "CA1716:Identifiers should not match keywords", Justification = "<Pending>", Scope = "member", Target = "~M:DrinkShop.Repository.IPurchaseRepository.UpdatePurchaseTime(System.Int32,System.DateTime)")]
[assembly: SuppressMessage("Design", "CA1012:Abstract types should not have public constructors", Justification = "<Pending>", Scope = "type", Target = "~T:DrinkShop.Repository.BRepository`1")]
