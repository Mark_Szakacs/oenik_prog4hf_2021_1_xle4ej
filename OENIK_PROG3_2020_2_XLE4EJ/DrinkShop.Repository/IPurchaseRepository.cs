﻿// <copyright file="IPurchaseRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrinkShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using DrinkShop.Data.Drinks;

    /// <summary>
    /// Intarface of Purchases.
    /// </summary>
    public interface IPurchaseRepository : IRepository<Purchase>
    {
        /// <summary>
        /// Create a new purchase.
        /// </summary>
        /// <param name="purchase">A new purchase to add.</param>
        void Create(Purchase purchase);

        /// <summary>
        /// Update the time of the purchase.
        /// </summary>
        /// <param name="id">id of the purchase.</param>
        /// <param name="date">new date of the purchase.</param>
        void UpdatePurchaseTime(int id, DateTime date);

        /// <summary>
        /// Deleting a purchase.
        /// </summary>
        /// <param name="id">The id of the purchase.</param>
        void Delete(int id);
    }
}
