﻿// <copyright file="BrandRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrinkShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DrinkShop.Data.Drinks;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Brandrepo class.
    /// </summary>
    public class BrandRepository : BRepository<Brand>, IBrandRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BrandRepository"/> class.
        /// </summary>
        /// <param name="ctx">name of dbcontext.</param>
        public BrandRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// Crate a brand.
        /// </summary>
        /// <param name="brand">Name of the brand.</param>
        public void Create(Brand brand)
        {
            this.ctx.Add(brand);
            this.ctx.SaveChanges();
        }

        /// <summary>
        /// Delete a brand.
        /// </summary>
        /// <param name="id">The id of the item.</param>
        public void Delete(int id)
        {
            this.ctx.Remove(this.GetOne(id));
            this.ctx.SaveChanges();
        }

        /// <summary>
        /// Update the name of the brand.
        /// </summary>
        /// <param name="id">id of the item.</param>
        /// <param name="name">New name of the brand.</param>
        public void UpdateName(int id, string name)
        {
            var brand = this.GetOne(id);
            brand.BrandName = name;
            this.ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public override Brand GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.BrandID == id);
        }
    }
}
