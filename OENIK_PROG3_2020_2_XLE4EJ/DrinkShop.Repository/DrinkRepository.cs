﻿// <copyright file="DrinkRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrinkShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DrinkShop.Data.Drinks;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// ejnkvdsjkvds.
    /// </summary>
    public class DrinkRepository : BRepository<DrinksT>, IDrinkRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DrinkRepository"/> class.
        /// akk.
        /// </summary>
        /// <param name="ctx">adsasdfvv.</param>
        public DrinkRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// Create a new drink.
        /// </summary>
        /// <param name="drinks">A new drink to add.</param>
        public void Create(DrinksT drinks)
        {
            this.ctx.Add(drinks);
            this.ctx.SaveChanges();
        }

        /// <summary>
        /// Deleting a drink.
        /// </summary>
        /// <param name="id">The id of the drink.</param>
        public void Delete(int id)
        {
            this.ctx.Remove(this.GetOne(id));
            this.ctx.SaveChanges();
        }

        /// <summary>
        /// Update the name of a drink.
        /// </summary>
        /// <param name="id">The id of a drink.</param>
        /// <param name="name">The new name of the drink.</param>
        public void UpdateName(int id, string name)
        {
            var drinks = this.GetOne(id);
            drinks.DrinkName = name;
            this.ctx.SaveChanges();
        }

        /// <summary>
        /// Update the price of a drink.
        /// </summary>
        /// <param name="id">The id of the drink.</param>
        /// <param name="price">The new price of the drink.</param>
        public void UpdatePrice(int id, int price)
        {
            var drinks = this.GetOne(id);
            drinks.Price = price;
            this.ctx.SaveChanges();
        }

        /// <summary>
        /// asdasfe.
        /// </summary>
        /// <param name="id">asdvdsvf.</param>
        /// <returns>...</returns>
        public override DrinksT GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.DrinkID == id);
        }
    }
}
