﻿// <copyright file="BRepository{T}.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrinkShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Main Repository.
    /// </summary>
    /// <typeparam name="T">Generic parameter.</typeparam>
    public abstract class BRepository<T> : IRepository<T>
        where T : class
    {
        /// <summary>
        /// asdds.
        /// </summary>
        private protected DbContext ctx;

        /// <summary>
        /// Initializes a new instance of the <see cref="BRepository{T}"/> class.
        /// aSd.
        /// </summary>
        /// <param name="ctx">adsdsa.</param>
        public BRepository(DbContext ctx)
        {
            this.ctx = ctx;
        }

        /// <summary>
        /// Returns every entities of the given type from the database.
        /// </summary>
        /// <returns>List of chosen entities.</returns>
        public IQueryable<T> GetAll()
        {
            return this.ctx.Set<T>();
        }

        /// <inheritdoc/>
        /// /// <summary>
        /// Returns one entity of the given type from the database.
        /// </summary>
        /// <param name="id">The id of the chosen entity.</param>
        /// <returns>The chosen entity.</returns>
        public abstract T GetOne(int id);

        /// <summary>
        /// Isert a new entity.
        /// </summary>
        /// <param name="entity">the new entity.</param>
        public void Insert(T entity)
        {
            if (!this.ctx.Set<T>().Contains(entity))
            {
                this.ctx.Set<T>().Add(entity);
                this.ctx.SaveChanges();
            }
        }

        /// <summary>
        /// Remove from database.
        /// </summary>
        /// <param name="id">the id of what we want to remove.</param>
        /// <returns>We van remove or not.</returns>
        public bool Remove(int id)
        {
            var entity = this.GetOne(id);
            if (entity == null)
            {
                return false;
            }
            else
            {
                this.ctx.Set<T>().Remove(entity);
                this.ctx.SaveChanges();
                return true;
            }
        }
    }
}
