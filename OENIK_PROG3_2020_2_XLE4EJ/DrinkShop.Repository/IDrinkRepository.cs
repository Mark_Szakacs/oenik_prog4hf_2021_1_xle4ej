﻿// <copyright file="IDrinkRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrinkShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using DrinkShop.Data.Drinks;

    /// <summary>
    /// Idrinkrepo Interface.
    /// </summary>
    public interface IDrinkRepository : IRepository<DrinksT>
    {
        /// <summary>
        /// Create a new drink.
        /// </summary>
        /// <param name="drinks">A new drink to add.</param>
        void Create(DrinksT drinks);

        /// <summary>
        /// Update the name of a drink.
        /// </summary>
        /// <param name="id">The id of a drink.</param>
        /// <param name="name">The new name of the drink.</param>
        void UpdateName(int id, string name);

        /// <summary>
        /// Update the price of a drink.
        /// </summary>
        /// <param name="id">The id of the drink.</param>
        /// <param name="price">The new price of the drink.</param>
        void UpdatePrice(int id, int price);

        /// <summary>
        /// Deleting a drink.
        /// </summary>
        /// <param name="id">The id of the drink.</param>
        void Delete(int id);
    }
}
