﻿// <copyright file="PurchaseRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrinkShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DrinkShop.Data.Drinks;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Repo of the purchases.
    /// </summary>
    public class PurchaseRepository : BRepository<Purchase>, IPurchaseRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PurchaseRepository"/> class.
        /// DbContext of the purchases repo.
        /// </summary>
        /// <param name="ctx">dbcontext name.</param>
        public PurchaseRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// Create a new purchase.
        /// </summary>
        /// <param name="purchase">A new purchase to add.</param>
        public void Create(Purchase purchase)
        {
            this.ctx.Add(purchase);
            this.ctx.SaveChanges();
        }

        /// <summary>
        /// Deleting a purchase.
        /// </summary>
        /// <param name="id">The id of the purchase.</param>
        public void Delete(int id)
        {
            this.ctx.Remove(this.GetOne(id));
            this.ctx.SaveChanges();
        }

        /// <summary>
        /// Returns one purchase from the database.
        /// </summary>
        /// <param name="id">The id of the purchase.</param>
        /// <returns>A purchase.</returns>
        public override Purchase GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.PurchaseID == id);
        }

        /// <summary>
        /// Update the time of the purchase.
        /// </summary>
        /// <param name="id">The id of the purchase.</param>
        /// <param name="date">The new date.</param>
        public void UpdatePurchaseTime(int id, DateTime date)
        {
            var purchase = this.GetOne(id);
            purchase.PurchaseDate = date;
            this.ctx.SaveChanges();
        }
    }
}