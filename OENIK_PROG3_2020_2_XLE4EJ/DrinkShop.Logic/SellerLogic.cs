﻿// <copyright file="SellerLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrinkShop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DrinkShop.Data.Drinks;
    using DrinkShop.Repository;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Seller Logic class.
    /// </summary>
    public class SellerLogic : ISellerLogic
    {
        private IDrinkRepository drinkRepo;
        private IBrandRepository brandRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="SellerLogic"/> class.
        /// repo of the drinks.
        /// </summary>
        /// <param name="drinkRepo">name of the repo.</param>
        public SellerLogic(IDrinkRepository drinkRepo)
        {
            this.drinkRepo = drinkRepo;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SellerLogic"/> class.
        /// repo of the brands.
        /// </summary>
        /// <param name="brandRepo">name of the repo.</param>
        public SellerLogic(IBrandRepository brandRepo)
        {
            this.brandRepo = brandRepo;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SellerLogic"/> class.
        /// repo of the brands.
        /// </summary>
        /// <param name="brandRepo">name of the repo.</param>
        /// <param name="drinkRepo">Drinkrepo.</param>
        public SellerLogic(IBrandRepository brandRepo, IDrinkRepository drinkRepo)
        {
            this.brandRepo = brandRepo;
            this.drinkRepo = drinkRepo;
        }

        // Create

        /// <summary>
        /// Create a drink.
        /// </summary>
        /// <param name="drinks">name of the drink.</param>
        public void CreateDrink(DrinksT drinks)
        {
            this.drinkRepo.Insert(drinks);
        }

        /// <summary>
        /// Create a brand.
        /// </summary>
        /// <param name="brand">name of the brand.</param>
        public void CreateBrand(Brand brand)
        {
            this.brandRepo.Insert(brand);
        }

        // Read

        /// <summary>
        /// Get all drink.
        /// </summary>
        /// <returns>List of drink.</returns>
        public IList<DrinksT> GetAllDrinks()
        {
            return this.drinkRepo.GetAll().ToList();
        }

        /// <summary>
        /// Get the id of a drink.
        /// </summary>
        /// <param name="id">id of the drink.</param>
        /// <returns>with the id of the drink.</returns>
        public DrinksT GetDrinkById(int id)
        {
            return this.drinkRepo.GetOne(id);
        }

        /// <summary>
        /// Get all brand.
        /// </summary>
        /// <returns>List of brand.</returns>
        public IList<Brand> GetAllBrand()
        {
            return this.brandRepo.GetAll().ToList();
        }

        /// <summary>
        /// Get the id of a brand.
        /// </summary>
        /// <param name="id">id of the brand.</param>
        /// <returns>with the id of the brand.</returns>
        public Brand GetBrandById(int id)
        {
            return this.brandRepo.GetOne(id);
        }

        /// <summary>
        /// Get drinks by brand.
        /// </summary>
        /// <param name="brand">brand name.</param>
        /// <returns>getall.</returns>
        public IList<DrinksT> GetDrinksbyBrand(int brand)
        {
            return this.drinkRepo.GetAll().Where(x => x.BrandID == brand).ToList();
        }

        // Update

        /// <summary>
        /// Update the name of a drink.
        /// </summary>
        /// <param name="id">The id of a drink.</param>
        /// <param name="name">The new name of the drink.</param>
        public void UpdateDrinkName(int id, string name)
        {
            this.drinkRepo.UpdateName(id, name);
        }

        /// <summary>
        /// Update the price of a drink.
        /// </summary>
        /// <param name="id">The id of the drink.</param>
        /// <param name="price">The new price of the drink.</param>
        public void UpdateDrinkPrice(int id, int price)
        {
            this.drinkRepo.UpdatePrice(id, price);
        }

        /// <summary>
        /// Update the name of a brand.
        /// </summary>
        /// <param name="id">The id of a brand.</param>
        /// <param name="brandname">The new id of a brand.</param>
        public void UpdateBrandName(int id, string brandname)
        {
            this.brandRepo.UpdateName(id, brandname);
        }

        // Delete

        /// <summary>
        /// Deleting a drink.
        /// </summary>
        /// <param name="id">The id of the drink.</param>
        public void DeleteDrink(int id)
        {
            this.drinkRepo.Remove(id);
        }

        /// <summary>
        /// Deleting a brand.
        /// </summary>
        /// <param name="id">The id of the brand.</param>
        public void DeleteBrand(int id)
        {
            this.brandRepo.Remove(id);
        }

        /// <summary>
        /// Strongdrinks Ilist.
        /// </summary>
        /// <param name="alc">alcprecent.</param>
        /// <returns>toList.</returns>
        public IList<StrongDrink> StrongDrinks(int alc)
        {
            var strong = this.drinkRepo.GetAll().Where(x => x.Alcpercent > alc).Select(x => new StrongDrink
            {
                BrandName = x.Brand.BrandName,
                DrinkName = x.DrinkName,
            });
            return strong.ToList();
        }

        /// <summary>
        /// Async ersion of Strongdrinks.
        /// </summary>
        /// <param name="alc">int alcprecent.</param>
        /// <returns>Task run.</returns>
        public Task<IList<StrongDrink>> StrongDrinksAsync(int alc)
        {
            return Task.Run(() => this.StrongDrinks(alc));
        }

        /// <summary>
        /// The most expensive drink.
        /// </summary>
        /// <returns>most exp drink.</returns>
        public DrinksT MostExpensiveDrink()
        {
            var exp = from d in this.drinkRepo.GetAll().ToList()
                      orderby d.Price descending
                      select d;
            return exp.First();
        }

        /// <summary>
        /// Most expensie Drink Async.
        /// </summary>
        /// <returns>Task run.</returns>
        public Task<DrinksT> MostExpensiveDrinkAsync()
        {
            return Task.Run(() => this.MostExpensiveDrink());
        }

        /// <summary>
        /// Get the average price of the dirnks.
        /// </summary>
        /// <returns>A list of drinks.</returns>
        public IList<AverageResult> GetPriveAvrage()
        {
            var q = from drinks in this.drinkRepo.GetAll()
                    group drinks by new { drinks.Brand.BrandID, drinks.Brand.BrandName } into a
                    select new AverageResult
                    {
                        BrandID = a.Key.BrandID,
                        Avg = a.Average(x => x.Price) ?? 0,
                        BrandName = a.Key.BrandName,
                    };
            return q.ToList();
        }

        /// <summary>
        /// Most expensie Drink Async.
        /// </summary>
        /// <returns>Task run.</returns>
        public Task<IList<AverageResult>> GetPriveAvrageAsync()
        {
            return Task.Run(() => this.GetPriveAvrage());
        }
    }
}
