﻿// <copyright file="ISellerLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrinkShop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading.Tasks;
    using DrinkShop.Data.Drinks;

    /// <summary>
    /// Isellerlogic interface.
    /// </summary>
    public interface ISellerLogic
    {
        // Create

        /// <summary>
        /// Create a drink.
        /// </summary>
        /// <param name="drinks">The name of the Drink.</param>
        void CreateDrink(DrinksT drinks);

        /// <summary>
        /// Crate a brand.
        /// </summary>
        /// <param name="brand">The name of the Brand.</param>
        void CreateBrand(Brand brand);

        /// <summary>
        /// Ilist of the Drinks.
        /// </summary>
        /// <returns>none.</returns>
        IList<DrinksT> GetAllDrinks();

        /// <summary>
        /// Get the id of the Drink.
        /// </summary>
        /// <param name="id">id of the drink.</param>
        /// <returns>id.</returns>
        DrinksT GetDrinkById(int id);

        /// <summary>
        /// Ilist of the brand.
        /// </summary>
        /// <returns>none.</returns>
        IList<Brand> GetAllBrand();

        /// <summary>
        /// Get the id of the Brand.
        /// </summary>
        /// <param name="id">id of the brand.</param>
        /// <returns>id.</returns>
        Brand GetBrandById(int id);

        // Delete

        /// <summary>
        /// Deleting a drink.
        /// </summary>
        /// <param name="id">The id of the drink.</param>
        void DeleteDrink(int id);

        /// <summary>
        /// Deleting a brand.
        /// </summary>
        /// <param name="id">The id of the brand.</param>
        void DeleteBrand(int id);

        // Update

        /// <summary>
        /// Update a brand name.
        /// </summary>
        /// <param name="id">name of the brand.</param>
        /// <param name="brandname">new name of the brand.</param>
        void UpdateBrandName(int id, string brandname);

        /// <summary>
        /// Update the price of a drink.
        /// </summary>
        /// <param name="id">The id of the drink.</param>
        /// <param name="price">The new price of the drink.</param>
        void UpdateDrinkPrice(int id, int price);

        /// <summary>
        /// Update the name of a drink.
        /// </summary>
        /// <param name="id">The id of a drink.</param>
        /// <param name="name">The new name of the drink.</param>
        void UpdateDrinkName(int id, string name);

        // Non-crud

        /// <summary>
        /// Returns a list of strong drink, where the alchpercent is more than 30%.
        /// </summary>
        /// <param name="alc">int alchprecent.</param>
        /// <returns>IList.</returns>
        IList<StrongDrink> StrongDrinks(int alc);

        /// <summary>
        /// Async Strong drink method.
        /// </summary>
        /// <param name="alc">alchprecent.</param>
        /// <returns>task run.</returns>
        public Task<IList<StrongDrink>> StrongDrinksAsync(int alc);

        /// <summary>
        /// Returns the most expensive Drink.
        /// </summary>
        /// <returns>The most expensive Drink.</returns>
        DrinksT MostExpensiveDrink();

        /// <summary>
        /// Get the average price of the drinks.
        /// </summary>
        /// <returns>With average price.</returns>
        IList<AverageResult> GetPriveAvrage();
    }
}
