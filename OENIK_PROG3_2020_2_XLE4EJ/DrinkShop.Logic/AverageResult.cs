﻿// <copyright file="AverageResult.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrinkShop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Class for average results.
    /// </summary>
    public class AverageResult
    {
        /// <summary>
        /// Gets or sets for drinkname.
        /// </summary>
        public int BrandID { get; set; }

        /// <summary>
        /// Gets or sets for Brandname.
        /// </summary>
        public string BrandName { get; set; }

        /// <summary>
        /// Gets or sets for avg.
        /// </summary>
        public double Avg { get; set; }

        /// <summary>
        /// Tostring.
        /// </summary>
        /// <returns>Drinkname and avg.</returns>
        public override string ToString()
        {
            return $"{this.BrandID}: {this.BrandName} : {this.Avg}";
        }

        /// <summary>
        /// Equals method.
        /// </summary>
        /// <param name="obj">object.</param>
        /// <returns>obj.</returns>
        public override bool Equals(object obj)
        {
            if (obj is AverageResult item)
            {
                return this.BrandID == item.BrandID && this.Avg == item.Avg && this.BrandName == item.BrandName;
            }

            return false;
        }

        /// <summary>
        /// Gethashcode method.
        /// </summary>
        /// <returns>gethashcode.</returns>
        public override int GetHashCode()
        {
            return this.BrandID.GetHashCode() + this.Avg.GetHashCode() + this.BrandName.GetHashCode();
        }
    }
}
