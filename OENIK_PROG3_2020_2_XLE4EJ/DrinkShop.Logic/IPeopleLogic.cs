﻿// <copyright file="IPeopleLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrinkShop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading.Tasks;
    using DrinkShop.Data.Drinks;

    /// <summary>
    /// INTERFACE.
    /// </summary>
    public interface IPeopleLogic
    {
        // Create

        /// <summary>
        /// Crate a customer.
        /// </summary>
        /// <param name="customer">name of the customer.</param>
        public void CreateCustomer(Customer customer);

        /// <summary>
        /// Crate a purchase.
        /// </summary>
        /// <param name="purchase">name of the purchase.</param>
        public void CreatePurchase(Purchase purchase);

        // Read

        /// <summary>
        /// Ilist of customer.
        /// </summary>
        /// <returns>none.</returns>
        IList<Customer> GetAllCustomer();

        /// <summary>
        /// Get the id of the customer.
        /// </summary>
        /// <param name="id">the id of the customer.</param>
        /// <returns>none.</returns>
        Customer GetCustomerById(int id);

        /// <summary>
        /// Get the id of the purchase.
        /// </summary>
        /// <param name="id">the id of the purchase.</param>
        /// <returns>none.</returns>
        Purchase GetPurchaseByID(int id);

        /// <summary>
        /// Ilist of purchase.
        /// </summary>
        /// <returns>none.</returns>
        IList<Purchase> GetAllPurchase();

        // Update

        /// <summary>
        /// Update a name of a customer.
        /// </summary>
        /// <param name="id">tThe id of the customer.</param>
        /// <param name="name">The new name of the customer.</param>
        void UpdateCustomerName(int id, string name);

        /// <summary>
        /// Update the address of a customer.
        /// </summary>
        /// <param name="id">The id of the customer.</param>
        /// <param name="location">The new address of the customer.</param>
        void UpdateLocation(int id, string location);

        /// <summary>
        /// Update the date of a purchase.
        /// </summary>
        /// <param name="id">The id of the purchase.</param>
        /// <param name="date">The new date of the purchase.</param>
        void UpdatePurchaseTime(int id, DateTime date);

        // Delete

        /// <summary>
        /// Deleting a customer.
        /// </summary>
        /// <param name="id">The id of the customer.</param>
        /// <returns>True if remove method succesful.</returns>
        bool DeleteCust(int id);

        /// <summary>
        /// Deleting a purchase.
        /// </summary>
        /// <param name="id">The id of the purchase.</param>
        void DeletePuch(int id);

        // Non - Crud

        /// <summary>
        /// OverageAsnyc.
        /// </summary>
        /// <returns>IList.</returns>
        public Task<IList<OverAge>> OverAgeAsync();

        /// <summary>
        /// Update all of customer.
        /// </summary>
        /// <param name="customer">entity.</param>
        void UpdateCustomer(Customer customer);

        /// <summary>
        /// Change customer.
        /// </summary>
        /// <param name="id">id of customer.</param>
        /// <param name="name">name of customer.</param>
        /// <param name="address">address of customer.</param>
        /// <param name="phone">phone number of customer.</param>
        /// <param name="age">customer age.</param>
        /// <returns>true if can change.</returns>
        public bool ChangeCustomer(int id, string name, string address, string phone, int age);
    }
}
