﻿// <copyright file="MostPurchase.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrinkShop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// MostPurchase class.
    /// </summary>
    public class MostPurchase
    {
        /// <summary>
        /// Gets or sets for customerid.
        /// </summary>
        public int CustomerID { get; set; }

        /// <summary>
        /// Gets or sets fpr CustomerName.
        /// </summary>
        public string CostumerName { get; set; }

        /// <summary>
        /// Tostring.
        /// </summary>
        /// <returns>Drinkname and avg.</returns>
        public override string ToString()
        {
            return $"{this.CustomerID} - {this.CostumerName}";
        }

        /// <summary>
        /// Equals method.
        /// </summary>
        /// <param name="obj">object.</param>
        /// <returns>obj.</returns>
        public override bool Equals(object obj)
        {
            if (obj is MostPurchase other)
            {
                return this.CustomerID == other.CustomerID && this.CostumerName == other.CostumerName;
            }

            return false;
        }

        /// <summary>
        /// Gethashcode method.
        /// </summary>
        /// <returns>gethashcode.</returns>
        public override int GetHashCode()
        {
            return this.CustomerID.GetHashCode();
        }
    }
}
