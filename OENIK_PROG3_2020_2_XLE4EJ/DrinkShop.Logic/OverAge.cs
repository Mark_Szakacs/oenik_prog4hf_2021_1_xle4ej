﻿// <copyright file="OverAge.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrinkShop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Overage class.
    /// </summary>
    public class OverAge
    {
        /// <summary>
        /// Gets or sets of CustomerName.
        /// </summary>
        public string CustomerName { get; set; }

        /// <summary>
        /// Gets or sets of CustomerName.
        /// </summary>
        public int Age { get; set; }

        /// <summary>
        /// Gets or sets of CustomerName.
        /// </summary>
        public DateTime PurchaseTime { get; set; }

        /// <summary>
        /// Tostring override.
        /// </summary>
        /// <returns>Tostring.</returns>
        public override string ToString()
        {
            return $"{this.CustomerName} - {this.Age} - {this.PurchaseTime}";
        }

        /// <summary>
        /// Equals override.
        /// </summary>
        /// <param name="obj">object.</param>
        /// <returns>equals.</returns>
        public override bool Equals(object obj)
        {
            if (obj is OverAge other)
            {
                return this.CustomerName == other.CustomerName && this.Age == other.Age && this.PurchaseTime == other.PurchaseTime;
            }

            return false;
        }

        /// <summary>
        /// Gethashcode method.
        /// </summary>
        /// <returns>gethashcode.</returns>
        public override int GetHashCode()
        {
            return this.CustomerName.GetHashCode() + this.Age.GetHashCode() + this.PurchaseTime.GetHashCode();
        }
    }
}
