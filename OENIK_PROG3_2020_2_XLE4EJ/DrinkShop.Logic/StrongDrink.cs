﻿// <copyright file="StrongDrink.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrinkShop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// StrongDrinks Class.
    /// </summary>
    public class StrongDrink
    {
        /// <summary>
        /// Gets or sets for brandname.
        /// </summary>
        public string BrandName { get; set; }

        /// <summary>
        /// Gets or sets for drinkname.
        /// </summary>
        public string DrinkName { get; set; }

        /// <summary>
        /// Tostring.
        /// </summary>
        /// <returns>Drinkname and brandname.</returns>
        public override string ToString()
        {
            return $"{this.BrandName} - {this.DrinkName}";
        }

        /// <summary>
        /// Equals method.
        /// </summary>
        /// <param name="obj">object.</param>
        /// <returns>obj.</returns>
        public override bool Equals(object obj)
        {
            if (obj is StrongDrink other)
            {
                return this.DrinkName == other.DrinkName && this.BrandName == other.BrandName;
            }

            return false;
        }

        /// <summary>
        /// Gethashcode method.
        /// </summary>
        /// <returns>gethashcode.</returns>
        public override int GetHashCode()
        {
            return this.BrandName.GetHashCode() + this.DrinkName.GetHashCode();
        }
    }
}
