﻿// <copyright file="PeopleLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrinkShop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DrinkShop.Data.Drinks;
    using DrinkShop.Repository;

    /// <summary>
    /// People Logic class.
    /// </summary>
    public class PeopleLogic : IPeopleLogic
    {
        private ICustomerRepository custRepo;
        private IPurchaseRepository purchRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="PeopleLogic"/> class.
        /// </summary>
        /// <param name="custrepo">customer repo.</param>
        /// <param name="purchrepo">purchase repo.</param>
        public PeopleLogic(ICustomerRepository custrepo, IPurchaseRepository purchrepo)
        {
            this.custRepo = custrepo;
            this.purchRepo = purchrepo;
        }

        // Create

        /// <summary>
        /// Crate a customer.
        /// </summary>
        /// <param name="customer">name of the customer.</param>
        public void CreateCustomer(Customer customer)
        {
            this.custRepo.Insert(customer);
        }

        /// <summary>
        /// Crate a purchase.
        /// </summary>
        /// <param name="purchase">name of the purchase.</param>
        public void CreatePurchase(Purchase purchase)
        {
            this.purchRepo.Insert(purchase);
        }

        // Read

        /// <summary>
        /// Get all customer.
        /// </summary>
        /// <returns>List of customers.</returns>
        public IList<Customer> GetAllCustomer()
        {
            return this.custRepo.GetAll().ToList();
        }

        /// <summary>
        /// Get the id of a customer.
        /// </summary>
        /// <param name="id">id of the customer.</param>
        /// <returns>with the id of the customer.</returns>
        public Customer GetCustomerById(int id)
        {
            return this.custRepo.GetOne(id);
        }

        /// <summary>
        /// Get all purchase.
        /// </summary>
        /// <returns>List of purchase.</returns>
        public IList<Purchase> GetAllPurchase()
        {
            return this.purchRepo.GetAll().ToList();
        }

        /// <summary>
        /// Get the id of a purchase.
        /// </summary>
        /// <param name="id">id of the purchase.</param>
        /// <returns>with the id of the purchase.</returns>
        public Purchase GetPurchaseByID(int id)
        {
            return this.purchRepo.GetOne(id);
        }

        // Update

        /// <inheritdoc/>
        public void UpdateCustomer(Customer customer)
        {
            this.custRepo.Update(customer);
        }

        /// <summary>
        /// Update a name of a customer.
        /// </summary>
        /// <param name="id">tThe id of the customer.</param>
        /// <param name="name">The new name of the customer.</param>
        public void UpdateCustomerName(int id, string name)
        {
            this.custRepo.UpdateName(id, name);
        }

        /// <summary>
        /// Update the address of a customer.
        /// </summary>
        /// <param name="id">The id of the customer.</param>
        /// <param name="location">The new address of the customer.</param>
        public void UpdateLocation(int id, string location)
        {
            this.custRepo.UpdateAddress(id, location);
        }

        /// <summary>
        /// Update the date of a purchase.
        /// </summary>
        /// <param name="id">The id of the customer.</param>
        /// <param name="date">The new date of the purchase.</param>
        public void UpdatePurchaseTime(int id, DateTime date)
        {
            this.purchRepo.UpdatePurchaseTime(id, date);
        }

        // Delete

        /// <summary>
        /// Deleting a customer.
        /// </summary>
        /// <param name="id">The id of the customer.</param>
        /// <returns>True is can remove.</returns>
        public bool DeleteCust(int id)
        {
            return this.custRepo.Remove(id);
        }

        /// <summary>
        /// Deleting a purchase.
        /// </summary>
        /// <param name="id">The id of the purchase.</param>
        public void DeletePuch(int id)
        {
            this.purchRepo.Remove(id);
        }

        /// <summary>
        /// Overage method.
        /// </summary>
        /// <returns>List with over aged peoples.</returns>
        public IList<OverAge> OverAge()
        {
            var q = from x in this.custRepo.GetAll()
                    group x by new { x.Age, x.CostumerName, x.Purchase.PurchaseDate } into a
                    where a.Key.Age > 18
                    select new OverAge
                    {
                       CustomerName = a.Key.CostumerName,
                       Age = a.Key.Age,
                       PurchaseTime = a.Key.PurchaseDate,
                    };
            return q.ToList();
        }

        /// <summary>
        /// Over Age Async.
        /// </summary>
        /// <returns>Task run.</returns>
        public Task<IList<OverAge>> OverAgeAsync()
        {
            return Task.Run(() => this.OverAge());
        }

        /// <inheritdoc/>
        public bool ChangeCustomer(int id, string name, string address, string phone, int age)
        {
            return this.custRepo.ChangeCustomer(id, name, address, phone, age);
        }
    }
}
