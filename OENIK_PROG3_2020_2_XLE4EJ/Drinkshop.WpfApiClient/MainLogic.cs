﻿// <copyright file="MainLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrinkShop.WpfApiClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Text.Json;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// MainLogic of Api.
    /// </summary>
    public class MainLogic : IMainLogic
    {
        private string url = "http://localhost:37105/customersApi/";
        private HttpClient client = new HttpClient();
        private JsonSerializerOptions jsonOptions = new JsonSerializerOptions(JsonSerializerDefaults.Web);

        /// <inheritdoc/>
        public void SendMassege(bool success)
        {
            string msg = success ? "Operation completed successfully" : "Operation failed";
            Messenger.Default.Send(msg, "CustomerResult");
        }

        /// <inheritdoc/>
        public List<CustomerVM> ApiGetCustomers()
        {
            string json = this.client.GetStringAsync(this.url + "all").Result;
            var list = JsonSerializer.Deserialize<List<CustomerVM>>(json, this.jsonOptions);
            return list;
        }

        /// <inheritdoc/>
        public void ApiDelCustomer(CustomerVM customer)
        {
            bool succes = false;
            if (customer != null)
            {
                string json = this.client.GetStringAsync(this.url + "del/" + customer.ID.ToString()).Result;
                JsonDocument doc = JsonDocument.Parse(json);
                succes = doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
            }

            this.SendMassege(succes);
        }

        /// <inheritdoc/>
        public bool ApiEditCustomer(CustomerVM customer, bool isEditing)
        {
            if (customer == null)
            {
                return false;
            }

            string myUrl = this.url + (isEditing ? "mod" : "add");

            Dictionary<string, string> postData = new Dictionary<string, string>();
            if (isEditing)
            {
                postData.Add("id", customer.ID.ToString());
            }

            postData.Add("Name", customer.Name);
            postData.Add("Address", customer.Address);
            postData.Add("Phone", customer.Phone);
            postData.Add("Age", customer.Age.ToString());

            string json = this.client.PostAsync(myUrl, new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
            JsonDocument doc = JsonDocument.Parse(json);

            return doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
        }

        /// <inheritdoc/>
        public void EditCustomer(CustomerVM customer, Func<CustomerVM, bool> editorFunc)
        {
            CustomerVM clone = new CustomerVM();
            if (customer != null)
            {
                clone.CopyFrom(customer);
            }

            bool? succes = editorFunc?.Invoke(clone);
            if (succes == true)
            {
                if (customer != null)
                {
                    succes = this.ApiEditCustomer(clone, true);
                }
                else
                {
                    succes = this.ApiEditCustomer(clone, false);
                }

                this.SendMassege(succes == true);
            }
        }
    }
}
