﻿// <copyright file="IMainLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrinkShop.WpfApiClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// ImainLogic interface.
    /// </summary>
    public interface IMainLogic
    {
        /// <summary>
        /// Send Massege if succes.
        /// </summary>
        /// <param name="success">bool.</param>
        public void SendMassege(bool success);

        /// <summary>
        /// Get all customer for Api.
        /// </summary>
        /// <returns>List of customers.</returns>
        public List<CustomerVM> ApiGetCustomers();

        /// <summary>
        /// Delete customer for Api.
        /// </summary>
        /// <param name="customer">customer.</param>
        public void ApiDelCustomer(CustomerVM customer);

        /// <summary>
        /// Edit customer for api.
        /// </summary>
        /// <param name="customer">customer.</param>
        /// <param name="isEditing">true or false.</param>
        /// <returns>bool.</returns>
        public bool ApiEditCustomer(CustomerVM customer, bool isEditing);

        /// <summary>
        /// Edit customer.
        /// </summary>
        /// <param name="customer">customervm customer.</param>
        /// <param name="editorFunc">bool.</param>
        public void EditCustomer(CustomerVM customer, Func<CustomerVM, bool> editorFunc);
    }
}
