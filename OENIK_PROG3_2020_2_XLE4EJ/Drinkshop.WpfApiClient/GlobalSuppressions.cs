﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Design", "CA1001:Types that own disposable fields should be disposable", Justification = "<Pending>", Scope = "type", Target = "~T:DrinkShop.WpfApiClient.MainLogic")]
[assembly: SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "<Pending>", Scope = "member", Target = "~M:DrinkShop.WpfApiClient.MainLogic.ApiGetCustomers~System.Collections.Generic.List{DrinkShop.WpfApiClient.CustomerVM}")]
[assembly: SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1404:Code analysis suppression should have justification", Justification = "<Pending>")]
[assembly: SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "<Pending>", Scope = "member", Target = "~M:DrinkShop.WpfApiClient.MainLogic.ApiDelCustomer(DrinkShop.WpfApiClient.CustomerVM)")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Pending>", Scope = "member", Target = "~M:DrinkShop.WpfApiClient.MainLogic.ApiEditCustomer(DrinkShop.WpfApiClient.CustomerVM,System.Boolean)~System.Boolean")]
[assembly: SuppressMessage("Reliability", "CA2000:Dispose objects before losing scope", Justification = "<Pending>", Scope = "member", Target = "~M:DrinkShop.WpfApiClient.MainLogic.ApiEditCustomer(DrinkShop.WpfApiClient.CustomerVM,System.Boolean)~System.Boolean")]
[assembly: SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "<Pending>", Scope = "member", Target = "~M:DrinkShop.WpfApiClient.MainLogic.ApiEditCustomer(DrinkShop.WpfApiClient.CustomerVM,System.Boolean)~System.Boolean")]
[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "<Pending>", Scope = "member", Target = "~M:DrinkShop.WpfApiClient.IMainLogic.ApiGetCustomers~System.Collections.Generic.List{DrinkShop.WpfApiClient.CustomerVM}")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "<Pending>", Scope = "member", Target = "~P:DrinkShop.WpfApiClient.MainVM.AllCustomer")]
[assembly: SuppressMessage("Design", "CA1812:Avoid uninstantiated internal classes", Justification = "False error.")]
