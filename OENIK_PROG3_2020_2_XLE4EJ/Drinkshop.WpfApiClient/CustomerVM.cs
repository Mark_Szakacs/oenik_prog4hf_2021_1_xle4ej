﻿// <copyright file="CustomerVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrinkShop.WpfApiClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// CustomerVM in api wpf.
    /// </summary>
    public class CustomerVM : ObservableObject
    {
        private int? iD;
        private string name;
        private int age;
        private string phone;
        private string address;

        /// <summary>
        /// Gets or sets customerID prop.
        /// </summary>
        public int? ID
        {
            get { return this.iD; }
            set { this.Set(ref this.iD, value); }
        }

        /// <summary>
        /// Gets or sets customerName prop.
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set { this.Set(ref this.name, value); }
        }

        /// <summary>
        /// Gets or sets Age prop.
        /// </summary>
        public int Age
        {
            get { return this.age; }
            set { this.Set(ref this.age, value); }
        }

        /// <summary>
        /// Gets or sets Phone prop.
        /// </summary>
        public string Phone
        {
            get { return this.phone; }
            set { this.Set(ref this.phone, value); }
        }

        /// <summary>
        /// Gets or sets customerAdress prop.
        /// </summary>
        public string Address
        {
            get { return this.address; }
            set { this.Set(ref this.address, value); }
        }

        /// <summary>
        /// Copyfrom.
        /// </summary>
        /// <param name="other">other.</param>
        public void CopyFrom(CustomerVM other)
        {
            this.GetType().GetProperties().ToList().ForEach(prop => prop.SetValue(this, prop.GetValue(other)));
        }
    }
}
