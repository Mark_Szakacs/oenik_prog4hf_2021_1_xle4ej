﻿// <copyright file="MainVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrinkShop.WpfApiClient
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;

    /// <summary>
    /// MainVM class.
    /// </summary>
    public class MainVM : ViewModelBase
    {
        private IMainLogic logic;
        private CustomerVM selectedCustomer;
        private ObservableCollection<CustomerVM> allCustomer;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainVM"/> class.
        /// </summary>
        /// <param name="logic">imainlogic.</param>
        public MainVM(IMainLogic logic)
        {
            this.logic = logic;

            this.LoadCmd = new RelayCommand(() => this.AllCustomer = new ObservableCollection<CustomerVM>(this.logic.ApiGetCustomers()));
            this.DelCmd = new RelayCommand(() => this.logic.ApiDelCustomer(this.selectedCustomer));
            this.AddCmd = new RelayCommand(() => this.logic.EditCustomer(null, this.EditorFunc));
            this.ModCmd = new RelayCommand(() => this.logic.EditCustomer(this.selectedCustomer, this.EditorFunc));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainVM"/> class.
        /// </summary>
        public MainVM()
            : this(
                IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<IMainLogic>())
        {
        }

        /// <summary>
        /// Gets or sets for allcustomer.
        /// </summary>
        public ObservableCollection<CustomerVM> AllCustomer
        {
            get { return this.allCustomer; }
            set { this.Set(ref this.allCustomer, value); }
        }

        /// <summary>
        /// Gets or sets for selectedcustomer.
        /// </summary>
        public CustomerVM SelectedCustomer
        {
            get { return this.selectedCustomer; }
            set { this.Set(ref this.selectedCustomer, value); }
        }

        /// <summary>
        /// Gets or sets for editorfunc.
        /// </summary>
        public Func<CustomerVM, bool> EditorFunc { get; set; }

        /// <summary>
        /// Gets for addcmd.
        /// </summary>
        public ICommand AddCmd { get; private set; }

        /// <summary>
        /// Gets for delcmd.
        /// </summary>
        public ICommand DelCmd { get; private set; }

        /// <summary>
        /// Gets for modcmd.
        /// </summary>
        public ICommand ModCmd { get; private set; }

        /// <summary>
        /// Gets for loadcmd.
        /// </summary>
        public ICommand LoadCmd { get; private set; }
    }
}
