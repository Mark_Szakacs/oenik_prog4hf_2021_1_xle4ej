﻿// <copyright file="CustomersController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrinkShop.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using DrinkShop.Logic;
    using DrinkShop.Web.Models;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Customer Controller class.
    /// </summary>
    public class CustomersController : Controller
    {
        private IPeopleLogic logic;
        private IMapper mapper;
        private CustomerListViewModel vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomersController"/> class.
        /// Constructor of customercontroller.
        /// </summary>
        /// <param name="logic">Ipeoplelogic.</param>
        /// <param name="mapper">Automapper.</param>
        public CustomersController(IPeopleLogic logic, IMapper mapper)
        {
            this.logic = logic;
            this.mapper = mapper;

            this.vm = new CustomerListViewModel();
            this.vm.EditedCustomer = new Models.Customer();

            var customers = logic.GetAllCustomer();
            this.vm.ListOfCustomers = mapper.Map<IList<Data.Drinks.Customer>, List<Models.Customer>>(customers);
        }

        /// <summary>
        /// Index of customers.
        /// </summary>
        /// <returns>customerindex vm.</returns>
        public IActionResult Index()
        {
            this.ViewData["editAction"] = "AddNew";
            return this.View("CustomerIndex", this.vm);
        }

        /// <summary>
        /// Details of one customer.
        /// </summary>
        /// <param name="id">id of customer.</param>
        /// <returns>Details.</returns>
        public IActionResult Details(int id)
        {
            return this.View("CustomersDetails", this.GetCustomerModel(id));
        }

        /// <summary>
        /// Remove a customer.
        /// </summary>
        /// <param name="id">id of the customer.</param>
        /// <returns>Redirect action index.</returns>
        public IActionResult Remove(int id)
        {
            this.TempData["editResult"] = "Delete Fail";
            if (this.logic.DeleteCust(id))
            {
                this.TempData["editResult"] = "Delete OK";
            }

            return this.RedirectToAction(nameof(this.Index));
        }

        /// <summary>
        /// Edit customer.
        /// </summary>
        /// <param name="id">id of the cutomer.</param>
        /// <returns>customerindex.</returns>
        public IActionResult Edit(int id)
        {
            this.ViewData["editAction"] = "Edit";
            this.vm.EditedCustomer = this.GetCustomerModel(id);
            return this.View("CustomerIndex", this.vm);
        }

        /// <summary>
        /// Edit method for adding a customer.
        /// </summary>
        /// <param name="customer">model customer.</param>
        /// <param name="editAction">edit action.</param>
        /// <returns>True if can edited.</returns>
        [HttpPost]
        public IActionResult Edit(Models.Customer customer, string editAction)
        {
            if (this.ModelState.IsValid && customer != null)
            {
                Data.Drinks.Customer customerEntity = this.mapper.Map<Models.Customer, Data.Drinks.Customer>(customer);
                this.TempData["editResult"] = "Edit OK";
                if (editAction == "AddNew")
                {
                    try
                    {
                        customerEntity.CostumerID = null;
                        this.logic.CreateCustomer(customerEntity);
                    }
                    catch (ArgumentException ex)
                    {
                        this.TempData["editResult"] = "AddCustomer FAIL: " + ex.Message;
                    }
                }
                else
                {
                    if (!this.logic.ChangeCustomer((int)customer.ID, customer.Name, customer.Address, customer.Phone, customer.Age))
                    {
                        this.TempData["editResult"] = "Edit FAIL";
                    }
                }

                return this.RedirectToAction(nameof(this.Index));
            }
            else
            {
                this.ViewData["editAction"] = "Edit";
                this.vm.EditedCustomer = customer;
                return this.View("CustomerIndex", this.vm);
            }
        }

        private Models.Customer GetCustomerModel(int id)
        {
            Data.Drinks.Customer oneCustomer = this.logic.GetCustomerById(id);
            return this.mapper.Map<Data.Drinks.Customer, Models.Customer>(oneCustomer);
        }
    }
}
