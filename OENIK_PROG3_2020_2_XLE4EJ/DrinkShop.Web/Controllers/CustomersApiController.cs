﻿// <copyright file="CustomersApiController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrinkShop.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using DrinkShop.Logic;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Customer API controller.
    /// </summary>
    public class CustomersApiController : Controller
    {
        private IPeopleLogic logic;
        private IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomersApiController"/> class.
        /// </summary>
        /// <param name="logic">Ipeople logic.</param>
        /// <param name="mapper">Imapper.</param>
        public CustomersApiController(IPeopleLogic logic, IMapper mapper)
        {
            this.logic = logic;
            this.mapper = mapper;
        }

        /// <summary>
        /// Get all customers.
        /// </summary>
        /// <returns>List of customers.</returns>
        [HttpGet]
        [ActionName("all")]
        public IEnumerable<Models.Customer> GetAll()
        {
            var customers = this.logic.GetAllCustomer();
            return this.mapper.Map<IList<Data.Drinks.Customer>, List<Models.Customer>>(customers);
        }

        /// <summary>
        /// Delet one Customer.
        /// </summary>
        /// <param name="id">Id of the customer.</param>
        /// <returns>deleted cust.</returns>
        [HttpGet]
        [ActionName("del")]
        public ApiResult DelOneCustomer(int id)
        {
            return new ApiResult() { OperationResult = this.logic.DeleteCust(id) };
        }

        /// <summary>
        /// Add one customer.
        /// </summary>
        /// <param name="customer">model customer.</param>
        /// <returns>succes true if added.</returns>
        [HttpPost]
        [ActionName("add")]
        public ApiResult AddOneCustomer(Models.Customer customer)
        {
            bool succes = true;
            Data.Drinks.Customer customerEntity = this.mapper.Map<Models.Customer, Data.Drinks.Customer>(customer);
            try
            {
                 this.logic.CreateCustomer(customerEntity);
            }
            catch (ArgumentException)
            {
                succes = false;
            }

            return new ApiResult() { OperationResult = succes };
        }

        /// <summary>
        /// Modify One Customer.
        /// </summary>
        /// <param name="customer">modifying customer.</param>
        /// <returns>new customer.</returns>
        [HttpPost]
        [ActionName("mod")]
        public ApiResult ModOneCustomer(Models.Customer customer)
        {
            return new ApiResult() { OperationResult = this.logic.ChangeCustomer((int)customer.ID, customer.Name, customer.Address, customer.Phone, customer.Age) };
        }
    }
}
