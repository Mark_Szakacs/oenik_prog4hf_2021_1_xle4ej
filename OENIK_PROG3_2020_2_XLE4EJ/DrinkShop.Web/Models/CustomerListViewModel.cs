﻿// <copyright file="CustomerListViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrinkShop.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// CustomerviewModel class.
    /// </summary>
    public class CustomerListViewModel
    {
        /// <summary>
        /// Gets or sets List of customer databese.
        /// </summary>
        public List<Customer> ListOfCustomers { get; set; }

        /// <summary>
        /// Gets or sets of Edited customer.
        /// </summary>
        public Customer EditedCustomer { get; set; }
    }
}
