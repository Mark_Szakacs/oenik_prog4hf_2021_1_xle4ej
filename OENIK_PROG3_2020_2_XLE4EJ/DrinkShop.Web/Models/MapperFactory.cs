﻿// <copyright file="MapperFactory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrinkShop.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;

    /// <summary>
    /// MapperFactory class.
    /// </summary>
    public class MapperFactory
    {
        /// <summary>
        /// Mapper.
        /// </summary>
        /// <returns>config of mapper.</returns>
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cnf =>
            {
                cnf.CreateMap<DrinkShop.Data.Drinks.Customer, DrinkShop.Web.Models.Customer>().
                ForMember(dest => dest.ID, map => map.MapFrom(src => src.CostumerID)).
                ForMember(dest => dest.Name, map => map.MapFrom(src => src.CostumerName)).
                ForMember(dest => dest.Address, map => map.MapFrom(src => src.CostumerAddress)).
                ForMember(dest => dest.Age, map => map.MapFrom(src => src.Age)).
                ForMember(dest => dest.Phone, map => map.MapFrom(src => src.Phone)).
                ForMember(dest => dest.PurchaseID, map => map.MapFrom(src => src.Purchase.PurchaseID)).
                ReverseMap();
            });
            return config.CreateMapper();
        }
    }
}
