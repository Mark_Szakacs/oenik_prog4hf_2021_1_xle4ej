﻿// <copyright file="Customer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrinkShop.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// customer web class.
    /// </summary>
    public class Customer
    {
        /// <summary>
        /// Gets or sets of id.
        /// </summary>
        [Display(Name = "Customer ID")]
        [Required]
        public int? ID { get; set; }

        /// <summary>
        /// Gets or sets of name.
        /// </summary>
        [Display(Name = "Customer Name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets of address.
        /// </summary>
        [Display(Name = "Customer Address")]
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets of age.
        /// </summary>
        [Display(Name = "Age")]
        public int Age { get; set; }

        /// <summary>
        /// Gets or sets of phone.
        /// </summary>
        [Display(Name = "Phone")]
        public string Phone { get; set; }

        /// <summary>
        /// Gets or sets of purchase id.
        /// </summary>
        [Display(Name = "purchaseID")]
        public int? PurchaseID { get; set; }
    }
}
