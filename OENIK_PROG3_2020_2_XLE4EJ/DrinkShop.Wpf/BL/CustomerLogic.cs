﻿// <copyright file="CustomerLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrinkShop.Wpf.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DrinkShop.Data.Drinks;
    using DrinkShop.Program;
    using DrinkShop.Wpf.Data;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// customerlogic.
    /// </summary>
    public class CustomerLogic : ICustomerLogic
    {
        private IEditorService editorService;
        private IMessenger messengerService;
        private Factory factory;

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerLogic"/> class.
        /// </summary>
        /// <param name="editorService">editorservice.</param>
        /// <param name="messengerService">messengerservice.</param>
        public CustomerLogic(IEditorService editorService, IMessenger messengerService)
        {
            this.editorService = editorService;
            this.messengerService = messengerService;
            this.factory = new Factory();
        }

        /// <summary>
        /// Add customer.
        /// </summary>
        /// <param name="list">list.</param>
        public void AddCustomer(IList<CustomerWpf> list)
        {
            CustomerWpf newCustomer = new CustomerWpf();
            if (this.editorService.EditCostumer(newCustomer) == true)
            {
                Customer customerEntity = newCustomer.CreateEntity(newCustomer);
                this.factory.PeLogic.CreateCustomer(customerEntity);

                list.Add(this.GetAllCustomer().Last());
                this.messengerService.Send("ADD OK", "LogicResult");
            }
            else
            {
                this.messengerService.Send("ADD Cancel", "LogicResult");
            }
        }

        /// <inheritdoc/>
        public IList<CustomerWpf> GetAllCustomer()
        {
            IList<Customer> customers = this.factory.PeLogic.GetAllCustomer().ToList();
            IList<CustomerWpf> customerModels = new List<CustomerWpf>();
            foreach (var entity in customers)
            {
                CustomerWpf customerM = new CustomerWpf();
                customerM = customerM.CreateModelFromEntity(entity);
                customerModels.Add(customerM);
            }

            return customerModels;
        }

        /// <summary>
        /// Delet customer.
        /// </summary>
        /// <param name="list"> list.</param>
        /// <param name="customer">customer.</param>
        public void DelCustomer(IList<CustomerWpf> list, CustomerWpf customer)
        {
            if (customer != null && list.Remove(customer))
            {
                Customer cust = this.factory.PeLogic.GetCustomerById(customer.CustomerID ?? 0);
                this.factory.PeLogic.DeleteCust(customer.CustomerID ?? 0);

                this.messengerService.Send("Delete Ok", "LogicResult");
            }
            else
            {
                this.messengerService.Send("Delete Failed", "LogicResult");
            }
        }

        /// <summary>
        /// Modify customer.
        /// </summary>
        /// <param name="customerToModify">modify the customer.</param>
        public void ModCustomer(CustomerWpf customerToModify)
        {
            if (customerToModify == null)
            {
                this.messengerService.Send("Edit Failed", "LogicResult");
            }

            CustomerWpf clone = new CustomerWpf();
            clone.CopyFrom(customerToModify);
            if (this.editorService.EditCostumer(clone) == true)
            {
                customerToModify.CopyFrom(clone);
                Customer customer = this.factory.PeLogic.GetCustomerById(customerToModify.CustomerID ?? 0);
                customer.CostumerName = clone.CustomerName;
                customer.CostumerAddress = clone.CustomerAddress;
                customer.Age = clone.Age;
                customer.Phone = clone.Phone;
                this.factory.PeLogic.UpdateCustomer(customer);
                this.messengerService.Send("MODIFY OK", "LogicResult");
            }
            else
            {
                this.messengerService.Send("MODIFY CANCEL", "LogicResult");
            }
        }
    }
}
