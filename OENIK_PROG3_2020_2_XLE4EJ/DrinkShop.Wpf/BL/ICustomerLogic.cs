﻿// <copyright file="ICustomerLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrinkShop.Wpf.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DrinkShop.Wpf.Data;

    /// <summary>
    /// IcustomerLogic interface.
    /// </summary>
    public interface ICustomerLogic
    {
        /// <summary>
        /// Add customer.
        /// </summary>
        /// <param name="list">list.</param>
        void AddCustomer(IList<CustomerWpf> list);

        /// <summary>
        /// Modify CUstomer.
        /// </summary>
        /// <param name="customerToModify">Customer to modify.</param>
        void ModCustomer(CustomerWpf customerToModify);

        /// <summary>
        /// Delete customer.
        /// </summary>
        /// <param name="list">lust.</param>
        /// <param name="customer">customer.</param>
        void DelCustomer(IList<CustomerWpf> list, CustomerWpf customer);

        /// <summary>
        /// GetAllCustomer.
        /// </summary>
        /// <returns>Ilist.</returns>
        IList<CustomerWpf> GetAllCustomer();
    }
}
