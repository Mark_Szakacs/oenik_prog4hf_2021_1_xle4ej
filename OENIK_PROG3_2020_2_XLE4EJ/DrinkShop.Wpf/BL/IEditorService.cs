﻿// <copyright file="IEditorService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrinkShop.Wpf.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DrinkShop.Wpf.Data;

    /// <summary>
    /// Ieditor iterface.
    /// </summary>
    public interface IEditorService
    {
        /// <summary>
        /// Editcustomer bool.
        /// </summary>
        /// <param name="c">c.</param>
        /// <returns>true.</returns>
        bool EditCostumer(CustomerWpf c);
    }
}
