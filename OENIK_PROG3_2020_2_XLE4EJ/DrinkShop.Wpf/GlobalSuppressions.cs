﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("StyleCop.CSharp.NamingRules", "SA1306:Field names should begin with lower-case letter", Justification = "<Pending>", Scope = "member", Target = "~F:DrinkShop.Wpf.UI.EditorWindow.VM")]
[assembly: SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1115:Parameter should follow comma", Justification = "<Pending>")]
[assembly: SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1404:Code analysis suppression should have justification", Justification = "<Pending>")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<Pending>", Scope = "member", Target = "~M:DrinkShop.Wpf.BL.CustomerLogic.DelCustomer(System.Collections.Generic.IList{DrinkShop.Wpf.Data.Customer},DrinkShop.Wpf.Data.Customer)")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<Pending>", Scope = "member", Target = "~M:DrinkShop.Wpf.BL.CustomerLogic.AddCustomer(System.Collections.Generic.IList{DrinkShop.Wpf.Data.Customer})")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<Pending>", Scope = "member", Target = "~M:DrinkShop.Wpf.BL.CustomerLogic.ModCustomer(DrinkShop.Wpf.Data.Customer)")]
[assembly: SuppressMessage("Performance", "CA1822:Mark members as static", Justification = "<Pending>", Scope = "member", Target = "~M:DrinkShop.Wpf.Data.CustomerWpf.CreateModelFromEntity(DrinkShop.Data.Drinks.Customer)~DrinkShop.Wpf.Data.CustomerWpf")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<Pending>", Scope = "member", Target = "~M:DrinkShop.Wpf.Data.CustomerWpf.CreateModelFromEntity(DrinkShop.Data.Drinks.Customer)~DrinkShop.Wpf.Data.CustomerWpf")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<Pending>", Scope = "member", Target = "~M:DrinkShop.Wpf.BL.CustomerLogic.DelCustomer(System.Collections.Generic.IList{DrinkShop.Wpf.Data.CustomerWpf},DrinkShop.Wpf.Data.CustomerWpf)")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<Pending>", Scope = "member", Target = "~M:DrinkShop.Wpf.BL.CustomerLogic.ModCustomer(DrinkShop.Wpf.Data.CustomerWpf)")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<Pending>", Scope = "member", Target = "~M:DrinkShop.Wpf.BL.CustomerLogic.AddCustomer(System.Collections.Generic.IList{DrinkShop.Wpf.Data.CustomerWpf})")]
[assembly: SuppressMessage("Performance", "CA1822:Mark members as static", Justification = "<Pending>", Scope = "member", Target = "~M:DrinkShop.Wpf.Data.CustomerWpf.CreateEntity(DrinkShop.Wpf.Data.CustomerWpf)~DrinkShop.Data.Drinks.Customer")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<Pending>", Scope = "member", Target = "~M:DrinkShop.Wpf.Data.CustomerWpf.CreateEntity(DrinkShop.Wpf.Data.CustomerWpf)~DrinkShop.Data.Drinks.Customer")]
