﻿// <copyright file="EditorViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrinkShop.Wpf.VM
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DrinkShop.Wpf.Data;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// EditorViewModell.
    /// </summary>
    public class EditorViewModel : ViewModelBase
    {
        private CustomerWpf customer;

        /// <summary>
        /// Initializes a new instance of the <see cref="EditorViewModel"/> class.
        /// Editorviewmodell.
        /// </summary>
        public EditorViewModel()
        {
            this.customer = new CustomerWpf();
            if (this.IsInDesignMode)
            {
                this.customer.CustomerName = "Proba Jóska";
                this.customer.CustomerAddress = "Try Street 15";
                this.customer.Age = 25;
                this.customer.Phone = "061112233";
            }
        }

        /// <summary>
        /// Gets or sets customer.
        /// </summary>
        public CustomerWpf Customer
        {
            get { return this.customer; }
            set { this.Set(ref this.customer, value); }
        }
    }
}
