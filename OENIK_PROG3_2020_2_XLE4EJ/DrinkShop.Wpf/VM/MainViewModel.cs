﻿// <copyright file="MainViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrinkShop.Wpf.VM
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using CommonServiceLocator;
    using DrinkShop.Program;
    using DrinkShop.Wpf.BL;
    using DrinkShop.Wpf.Data;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;

    /// <summary>
    /// MainViewModel.
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private ICustomerLogic logic;
        private CustomerWpf customerSelected;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        /// <param name="logic">customer.</param>
        public MainViewModel(ICustomerLogic logic)
        {
            this.logic = logic;

            this.Customers = new ObservableCollection<CustomerWpf>();

            if (this.IsInDesignMode)
            {
                CustomerWpf c1 = new CustomerWpf() { CustomerName = "AS SDA" };
                CustomerWpf c2 = new CustomerWpf() { CustomerName = "Aasdsad eq2w" };

                this.Customers.Add(c1);
                this.Customers.Add(c2);
            }
            else
            {
                foreach (var item in this.logic.GetAllCustomer())
                {
                    CustomerWpf newCustomer = new CustomerWpf() { CustomerName = item.CustomerName, CustomerAddress = item.CustomerAddress, Age = item.Age, Phone = item.Phone };
                    this.Customers.Add(item);
                }
            }

            this.Addcmd = new RelayCommand(() => this.logic.AddCustomer(this.Customers));
            this.Modcmd = new RelayCommand(() => this.logic.ModCustomer(this.CustomerSelected));
            this.Delcmd = new RelayCommand(() => this.logic.DelCustomer(this.Customers, this.CustomerSelected));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        public MainViewModel()
            : this(
                IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<ICustomerLogic>())
        {
        }

        /// <summary>
        /// Gets customers.
        /// </summary>
        public ObservableCollection<CustomerWpf> Customers { get; private set; }

       /// <summary>
       /// Gets or sets customerSelected.
       /// </summary>
        public CustomerWpf CustomerSelected
        {
            get { return this.customerSelected; }
            set { this.Set(ref this.customerSelected, value); }
        }

        /// <summary>
        /// Gets addcmd.
        /// </summary>
        public ICommand Addcmd { get; private set; }

        /// <summary>
        /// Gets modcmd.
        /// </summary>
        public ICommand Modcmd { get; private set; }

        /// <summary>
        /// Gets delcmd.
        /// </summary>
        public ICommand Delcmd { get; private set; }
    }
}
