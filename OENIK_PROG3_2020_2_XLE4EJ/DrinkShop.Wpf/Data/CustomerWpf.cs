﻿// <copyright file="CustomerWpf.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrinkShop.Wpf.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DrinkShop.Data.Drinks;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Customer class.
    /// </summary>
    public class CustomerWpf : ObservableObject
    {
        private int? customerID;
        private string customerName;
        private int age;
        private string phone;
        private string customerAddress;

        /// <summary>
        /// Gets or sets customerID prop.
        /// </summary>
        public int? CustomerID
        {
            get { return this.customerID; }
            set { this.Set(ref this.customerID, value); }
        }

        /// <summary>
        /// Gets or sets customerName prop.
        /// </summary>
        public string CustomerName
        {
            get { return this.customerName; }
            set { this.Set(ref this.customerName, value); }
        }

        /// <summary>
        /// Gets or sets Age prop.
        /// </summary>
        public int Age
        {
            get { return this.age; }
            set { this.Set(ref this.age, value); }
        }

        /// <summary>
        /// Gets or sets Phone prop.
        /// </summary>
        public string Phone
        {
            get { return this.phone; }
            set { this.Set(ref this.phone, value); }
        }

        /// <summary>
        /// Gets or sets customerAdress prop.
        /// </summary>
        public string CustomerAddress
        {
            get { return this.customerAddress; }
            set { this.Set(ref this.customerAddress, value); }
        }

        /// <summary>
        /// Create entity from wpf.
        /// </summary>
        /// <param name="newEntity">new entity.</param>
        /// <returns>cutsomet.</returns>
        public Customer CreateEntity(CustomerWpf newEntity)
        {
            Customer customer = new Customer();
            customer.CostumerName = newEntity.CustomerName;
            customer.CostumerAddress = newEntity.CustomerAddress;
            customer.Age = newEntity.Age;
            customer.Phone = newEntity.Phone;
            return customer;
        }

        /// <summary>
        /// create modell entity.
        /// </summary>
        /// <param name="customerEntity">cuts.</param>
        /// <returns>new entity.</returns>
        public CustomerWpf CreateModelFromEntity(Customer customerEntity)
        {
            CustomerWpf customer = new CustomerWpf();
            customer.CustomerID = customerEntity.CostumerID;
            customer.CustomerAddress = customerEntity.CostumerAddress;
            customer.CustomerName = customerEntity.CostumerName;
            customer.Phone = customerEntity.Phone;
            customer.Age = customerEntity.Age;
            return customer;
        }

        /// <summary>
        /// Copyfrom.
        /// </summary>
        /// <param name="other">other.</param>
        public void CopyFrom(CustomerWpf other)
        {
            this.GetType().GetProperties().ToList().ForEach(prop => prop.SetValue(this, prop.GetValue(other)));
        }
    }
}
