﻿// <copyright file="App.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrinkShop.Wpf
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows;
    using CommonServiceLocator;
    using DrinkShop.Data.Drinks;
    using DrinkShop.Logic;
    using DrinkShop.Program;
    using DrinkShop.Repository;
    using DrinkShop.Wpf.BL;
    using DrinkShop.Wpf.UI;
    using GalaSoft.MvvmLight.Messaging;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Interaction logic for App.xaml.
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="App"/> class.
        /// </summary>
        public App()
        {
            ServiceLocator.SetLocatorProvider(() => MyIoc.Instance);
            MyIoc.Instance.Register<IEditorService, EditorServiceViaWindow>();
            MyIoc.Instance.Register<IMessenger>(() => Messenger.Default);
            MyIoc.Instance.Register<ICustomerLogic, CustomerLogic>();
            MyIoc.Instance.Register<Factory, Factory>();
            MyIoc.Instance.Register<ICustomerRepository, CustomerRepository>();
            MyIoc.Instance.Register<IPurchaseRepository, PurchaseRepository>();
            MyIoc.Instance.Register<IDrinkRepository, DrinkRepository>();
            MyIoc.Instance.Register<IBrandRepository, BrandRepository>();
            MyIoc.Instance.Register<IPeopleLogic, PeopleLogic>();
            MyIoc.Instance.Register<DbContext, DrinksDBContext>();
        }
    }
}
