﻿// <copyright file="EditorServiceViaWindow.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrinkShop.Wpf.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DrinkShop.Wpf.BL;
    using DrinkShop.Wpf.Data;

    /// <summary>
    /// EditorService.
    /// </summary>
    public class EditorServiceViaWindow : IEditorService
    {
        /// <summary>
        /// Edit customer.
        /// </summary>
        /// <param name="c">c.</param>
        /// <returns>showdialod.</returns>
        public bool EditCostumer(CustomerWpf c)
        {
            EditorWindow win = new EditorWindow(c);
            return win.ShowDialog() ?? false;
        }
    }
}
