﻿// <copyright file="Menu.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrinkShop.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Castle.DynamicProxy.Generators.Emitters.SimpleAST;
    using ConsoleTools;
    using DrinkShop.Data.Drinks;
    using DrinkShop.Logic;
    using DrinkShop.Repository;

    /// <summary>
    /// CLass of the Menu.
    /// </summary>
    public class Menu
    {
        /// <summary>
        /// asd.
        /// </summary>
        private Factory factory = new Factory();

        /// <summary>
        /// Initializes a new instance of the <see cref="Menu"/> class.
        /// Invite the main menu.
        /// </summary>
        public Menu()
        {
            DrinksDBContext ctx = new DrinksDBContext();
            DrinkRepository repo = new DrinkRepository(ctx);
            this.MainMenu();
        }

        /// <summary>
        /// Main menu method.
        /// </summary>
        public void MainMenu()
        {
            string input = string.Empty;
            do
            {
                Console.WriteLine("*********Drink Shop*********");
                Console.WriteLine("1) Drinks CRUD methods");
                Console.WriteLine("2) Brand CRUD methods");
                Console.WriteLine("3) Customer CRUD methods");
                Console.WriteLine("4) Purchase CRUD methods");
                Console.WriteLine("5) Others");
                Console.WriteLine("6) Async");
                Console.WriteLine("q) Exit");
                input = Console.ReadLine();

                if (input == "1")
                {
                    Console.Clear();
                    this.DrinkMenu();
                }
                else if (input == "2")
                {
                    Console.Clear();
                    this.BrandMenu();
                }
                else if (input == "3")
                {
                    Console.Clear();
                    this.CustomerMenu();
                }
                else if (input == "4")
                {
                    Console.Clear();
                    this.PurchaseMenu();
                }
                else if (input == "5")
                {
                    Console.Clear();
                    this.OtherMenu();
                }
                else if (input == "6")
                {
                    Console.Clear();
                    this.AsyncMenu();
                }
                else
                {
                    if (input == "q")
                    {
                        Console.WriteLine("You will now quit!");
                    }
                    else
                    {
                        Console.WriteLine("Wrong input!");
                    }
                }
            }
            while (input != "q");
        }

        private static int IDSearch()
        {
            Console.WriteLine("Enter ID of the object: ");
            int id = int.Parse(Console.ReadLine());
            return id;
        }

        private static void Done(string msg)
        {
            Console.WriteLine(msg);
            Console.WriteLine("Press a button to continue");
            Console.ReadKey();
            Console.Clear();
        }

        private void CreateDrink()
        {
            DrinksT drinks = new DrinksT();
            Console.Write("Name: ");
            drinks.DrinkName = Console.ReadLine();
            Console.Write("Brand ID: ");
            drinks.BrandID = int.Parse(Console.ReadLine());
            Console.Write("Type: ");
            drinks.Type = Console.ReadLine();
            Console.Write("Alcpercent ");
            drinks.Alcpercent = int.Parse(Console.ReadLine());
            Console.Write("Price: ");
            drinks.Price = int.Parse(Console.ReadLine());
            this.factory.Selogic.CreateDrink(drinks);
        }

        private void CreateBrand()
        {
            Brand brand = new Brand();
            Console.Write("Brand name: ");
            brand.BrandName = Console.ReadLine();
            this.factory.Selogic.CreateBrand(brand);
        }

        private void CreatePurchase()
        {
            Purchase purchase = new Purchase();
            Console.Write("Date: ");
            purchase.PurchaseDate = DateTime.Parse(Console.ReadLine());
            Console.Write("Customer Name: ");
            purchase.CustomerName = Console.ReadLine();
            this.factory.PeLogic.CreatePurchase(purchase);
        }

        private void CreateCustomer()
        {
            Customer customer = new Customer();
            Console.Write("Name: ");
            customer.CostumerName = Console.ReadLine();
            Console.Write("Age: ");
            customer.Age = int.Parse(Console.ReadLine());
            Console.Write("Phone number: ");
            customer.Phone = Console.ReadLine();
            Console.Write("Address: ");
            customer.CostumerAddress = Console.ReadLine();
            Console.WriteLine("Purchase ID");
            customer.PurchaseID = int.Parse(Console.ReadLine());
            this.factory.PeLogic.CreateCustomer(customer);
        }

        private void ReadDrink()
        {
            foreach (var item in this.factory.Selogic.GetAllDrinks())
            {
                Console.WriteLine(item.DrinkID + " - " + item.DrinkName + " - " + item.Brand + " - type: " + item.Type + " - Price: " + item.Price + " - alc: " + item.Alcpercent);
            }
        }

        private void ReadBrand()
        {
            foreach (var item in this.factory.Selogic.GetAllBrand())
            {
                Console.WriteLine("Brand ID: " + item.BrandID + " - " + item.BrandName);
            }
        }

        private void ReadCustomer()
        {
            foreach (var item in this.factory.PeLogic.GetAllCustomer())
            {
                Console.WriteLine("Customer ID: " + item.CostumerID + " - " + item.CostumerName + " - Phone: " + item.Phone + " - Address: " + item.CostumerAddress + " Age " + item.Age);
            }
        }

        private void ReadPurchase()
        {
            foreach (var item in this.factory.PeLogic.GetAllPurchase())
            {
                Console.WriteLine("Purchase ID: " + item.PurchaseID + " - Date: " + item.PurchaseDate + " - Customer Name: " + item.CustomerName);
            }
        }

        private void DrinkMenu()
        {
            string input2 = string.Empty;

            do
            {
                Console.WriteLine("1) Create a Drink");
                Console.WriteLine("2) Read Drink");
                Console.WriteLine("3) Edit Drink name");
                Console.WriteLine("4) Edit Drink Price");
                Console.WriteLine("5) Delete Drink");
                Console.WriteLine("q) Exit");
                input2 = Console.ReadLine();

                if (input2 == "1")
                {
                    this.CreateDrink();
                    Done("Drink created");
                }
                else if (input2 == "2")
                {
                    this.ReadDrink();
                    Done(string.Empty);
                }
                else if (input2 == "3")
                {
                    this.UpdateDrinkName(IDSearch());
                    Done("Name changed");
                }
                else if (input2 == "4")
                {
                    this.UpdateDrinkPrice(IDSearch());
                    Done("Price updated");
                }
                else if (input2 == "5")
                {
                    this.DeleteDrink();
                    Done("Item deleted!");
                }
                else
                {
                    if (input2 == "q")
                    {
                        Console.WriteLine("Exiting...");
                    }
                    else
                    {
                        Console.WriteLine("Wrong input!");
                    }
                }
            }
            while (input2 != "q");
        }

        private void DeleteDrink()
        {
            Console.WriteLine("Enter the ID of the Drink: ");
            this.factory.Selogic.DeleteDrink(int.Parse(Console.ReadLine()));
        }

        private void DeleteBrand()
        {
            Console.WriteLine("Enter the ID of the Brand: ");
            this.factory.Selogic.DeleteBrand(int.Parse(Console.ReadLine()));
        }

        private void DeleteCustomer()
        {
            Console.WriteLine("Enter the ID of the customer: ");
            this.factory.PeLogic.DeleteCust(int.Parse(Console.ReadLine()));
        }

        private void DeletePurchase()
        {
            Console.WriteLine("Enter the ID of the purchase: ");
            this.factory.PeLogic.DeletePuch(int.Parse(Console.ReadLine()));
        }

        private void UpdateCustomerName(int id)
        {
            Console.WriteLine("Enter the new name: ");
            this.factory.PeLogic.UpdateCustomerName(id, Console.ReadLine());
        }

        private void UpdateCustomerAddress(int id)
        {
            Console.WriteLine("Enter the new address: ");
            this.factory.PeLogic.UpdateLocation(id, Console.ReadLine());
        }

        private void UpdateBrandName(int id)
        {
            Console.WriteLine("Enter the new name: ");
            this.factory.Selogic.UpdateBrandName(id, Console.ReadLine());
        }

        private void UpdatePurchaseTime(int id)
        {
            Console.WriteLine("Enter the new time: ");
            this.factory.PeLogic.UpdatePurchaseTime(id, DateTime.Parse(Console.ReadLine()));
        }

        private void UpdateDrinkName(int id)
        {
            Console.WriteLine("Enter the new name: ");
            this.factory.Selogic.UpdateDrinkName(id, Console.ReadLine());
        }

        private void UpdateDrinkPrice(int id)
        {
            Console.WriteLine("Enter the new price: ");
            this.factory.Selogic.UpdateDrinkPrice(id, int.Parse(Console.ReadLine()));
        }

        private void BrandMenu()
        {
            string input2 = string.Empty;
            do
            {
                Console.WriteLine("1) Create a Brand");
                Console.WriteLine("2) Read Brand");
                Console.WriteLine("3) Edit Brand Name");
                Console.WriteLine("4) Delete Brand");
                Console.WriteLine("q) Exit");
                input2 = Console.ReadLine();

                if (input2 == "1")
                {
                    this.CreateBrand();
                    Done("Brand created");
                }
                else if (input2 == "2")
                {
                    this.ReadBrand();
                    Done(string.Empty);
                }
                else if (input2 == "3")
                {
                    this.UpdateBrandName(IDSearch());
                    Done("Name updated");
                }
                else if (input2 == "4")
                {
                    this.DeleteBrand();
                    Done("Item deleted!");
                }
                else
                {
                    if (input2 == "q")
                    {
                        Console.WriteLine("Exiting...");
                    }
                    else
                    {
                        Console.WriteLine("Wrong input!");
                    }
                }
            }
            while (input2 != "q");
        }

        private void CustomerMenu()
        {
            string input2 = string.Empty;
            do
            {
                Console.WriteLine("1) Create a Customer");
                Console.WriteLine("2) Read Customer");
                Console.WriteLine("3) Edit Customer name");
                Console.WriteLine("4) Edit Customer address");
                Console.WriteLine("5) Delete Customer");
                Console.WriteLine("q) Exit");
                input2 = Console.ReadLine();

                if (input2 == "1")
                {
                    this.CreateCustomer();
                    Done("Customer created");
                }
                else if (input2 == "2")
                {
                    this.ReadCustomer();
                    Done(string.Empty);
                }
                else if (input2 == "3")
                {
                    this.UpdateCustomerName(IDSearch());
                    Done("Name changed");
                }
                else if (input2 == "4")
                {
                    this.UpdateCustomerAddress(IDSearch());
                    Done("Address updated");
                }
                else if (input2 == "5")
                {
                    this.DeleteCustomer();
                    Done("Item deleted!");
                }
                else
                {
                    if (input2 == "q")
                    {
                        Console.WriteLine("Exiting...");
                    }
                    else
                    {
                        Console.WriteLine("Wrong input!");
                    }
                }
            }
            while (input2 != "q");
        }

        private void PurchaseMenu()
        {
            string input2 = string.Empty;

            do
            {
                Console.WriteLine("1) Create a Purchase");
                Console.WriteLine("2) Read Purchase");
                Console.WriteLine("3) Edit Time");
                Console.WriteLine("4) Delete Purchase");
                Console.WriteLine("q) Exit");
                input2 = Console.ReadLine();

                if (input2 == "1")
                {
                    this.CreatePurchase();
                    Done("Purchase created");
                }
                else if (input2 == "2")
                {
                    this.ReadPurchase();
                    Done(string.Empty);
                }
                else if (input2 == "3")
                {
                    this.UpdatePurchaseTime(IDSearch());
                    Done("Time chaned");
                }
                else if (input2 == "4")
                {
                    this.DeletePurchase();
                    Done("Item deleted!");
                }
                else
                {
                    if (input2 == "q")
                    {
                        Console.WriteLine("Exiting...");
                    }
                    else
                    {
                        Console.WriteLine("Wrong input!");
                    }
                }
            }
            while (input2 != "q");
        }

        private void OtherMenu()
        {
            string input2 = string.Empty;
            do
            {
                Console.WriteLine("1) Strong drinks");
                Console.WriteLine("2) Most expensive drink");
                Console.WriteLine("3) Avrage Prices");
                Console.WriteLine("4) Over Age");

                Console.WriteLine("q) Exit");
                input2 = Console.ReadLine();

                if (input2 == "1")
                {
                    this.StrongDrinks();
                    Done(string.Empty);
                }
                else if (input2 == "2")
                {
                     this.MostExpensive();
                     Done(string.Empty);
                }
                else if (input2 == "3")
                {
                    this.AvgPrice();
                    Done(string.Empty);
                }
                else if (input2 == "4")
                {
                    this.OverAge();
                    Done(string.Empty);
                }
                else
                {
                    if (input2 == "q")
                    {
                        Console.WriteLine("Exiting...");
                    }
                    else
                    {
                        Console.WriteLine("Wrong input!");
                    }
                }
            }
            while (input2 != "q");
        }

        private void AsyncMenu()
        {
            string input2 = string.Empty;
            do
            {
                Console.WriteLine("1) Strong drinks Async");
                Console.WriteLine("2) Most expensive drink Async");
                Console.WriteLine("3) Async Avrage Prices");
                Console.WriteLine("4) Async Over Age");
                Console.WriteLine("q) Exit");
                input2 = Console.ReadLine();

                if (input2 == "1")
                {
                    this.StrongDrinkAsync();
                    Done(string.Empty);
                }
                else if (input2 == "2")
                {
                    this.MostExpensiveAsync();
                    Done(string.Empty);
                }
                else if (input2 == "3")
                {
                    this.AvgPriceAsync();
                    Done(string.Empty);
                }
                else if (input2 == "4")
                {
                    this.OverAgeAsync();
                    Done(string.Empty);
                }
                else
                {
                    if (input2 == "q")
                    {
                        Console.WriteLine("Exiting...");
                    }
                    else
                    {
                        Console.WriteLine("Wrong input!");
                    }
                }
            }
            while (input2 != "q");
        }

        private void StrongDrinks()
        {
            IList<StrongDrink> drinks = this.factory.Selogic.StrongDrinks(30);
            Console.WriteLine("List of strong drinks: ");
            foreach (var drink in drinks)
            {
                Console.WriteLine(drink);
            }
        }

        private void StrongDrinkAsync()
        {
            var res = this.factory.Selogic.StrongDrinksAsync(30).Result;
            foreach (var item in res)
            {
                Console.WriteLine(item);
            }
        }

        private void MostExpensive()
        {
            DrinksT drink = this.factory.Selogic.MostExpensiveDrink();
            Console.WriteLine("The most expensive drink: ");
            Console.WriteLine(drink.DrinkID + " " + drink.DrinkName + " " + drink.Price);
        }

        private void MostExpensiveAsync()
        {
            var res = this.factory.Selogic.MostExpensiveDrinkAsync().Result;
            Console.WriteLine("The most expensive drink: ");
            Console.WriteLine(res);
        }

        private void AvgPrice()
        {
            foreach (var item in this.factory.Selogic.GetPriveAvrage())
            {
                Console.WriteLine(item);
            }
        }

        private void AvgPriceAsync()
        {
            var res = this.factory.Selogic.GetPriveAvrageAsync().Result;
            foreach (var item in res)
            {
                Console.WriteLine(item);
            }
        }

        private void OverAge()
        {
            IList<OverAge> over = this.factory.PeLogic.OverAge();
            Console.WriteLine("List of Over Age: ");
            foreach (var ov in over)
            {
                Console.WriteLine(ov);
            }
        }

        private void OverAgeAsync()
        {
            var res = this.factory.PeLogic.OverAgeAsync().Result;
            foreach (var item in res)
            {
                Console.WriteLine(item);
            }
        }
    }
}
