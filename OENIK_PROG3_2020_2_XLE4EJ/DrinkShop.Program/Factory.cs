﻿// <copyright file="Factory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrinkShop.Program
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using DrinkShop.Data.Drinks;
    using DrinkShop.Logic;
    using DrinkShop.Repository;

    /// <summary>
    /// Factory class.
    /// </summary>
    public class Factory
    {
        /// <summary>
        /// Ctx.
        /// </summary>
        private static DrinksDBContext ctx = new DrinksDBContext();

        /// <summary>
        /// Initializes a new instance of the <see cref="Factory"/> class.
        /// </summary>
        public Factory()
        {
            this.Brandrep = new BrandRepository(ctx);
            this.DrinkRepo = new DrinkRepository(ctx);
            this.Custumerrepo = new CustomerRepository(ctx);
            this.Purchaserepo = new PurchaseRepository(ctx);
            this.Selogic = new SellerLogic(this.Brandrep, this.DrinkRepo);
            this.PeLogic = new PeopleLogic(this.Custumerrepo, this.Purchaserepo);
        }

        /// <summary>
        /// Gets or sets of brandlogic.
        /// </summary>
        public SellerLogic Selogic { get; set; }

        /// <summary>
        /// Gets or sets of Drinklogic.
        /// </summary>
        public SellerLogic Drinklogic { get; set; }

        /// <summary>
        /// Gets or sets of Drinklogic.
        /// </summary>
        public PeopleLogic Custlogic { get; set; }

        /// <summary>
        /// Gets or sets of Drinklogic.
        /// </summary>
        public PeopleLogic PeLogic { get; set; }

        /// <summary>
        /// Gets or sets of Brandlogic.
        /// </summary>
        public BrandRepository Brandrep { get; set; }

        /// <summary>
        /// Gets or sets of DrinkRepo.
        /// </summary>
        public DrinkRepository DrinkRepo { get; set; }

        /// <summary>
        /// Gets or sets of CutomerRepo.
        /// </summary>
        public CustomerRepository Custumerrepo { get; set; }

        /// <summary>
        /// Gets or sets of PurchRepo.
        /// </summary>
        public PurchaseRepository Purchaserepo { get; set; }
    }
}
