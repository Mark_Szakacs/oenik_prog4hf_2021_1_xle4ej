﻿// <copyright file="SellerTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrinkShop.Logic.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DrinkShop.Data.Drinks;
    using DrinkShop.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Sellertest.
    /// </summary>
    [TestFixture]
    public class SellerTest
    {
        private Mock<IDrinkRepository> drinkmock;
        private Mock<IBrandRepository> brandmock;
        private SellerLogic testlogic;

        /// <summary>
        /// Setup test.
        /// </summary>
        [SetUp]
        public void SetupTest()
        {
            this.brandmock = new Mock<IBrandRepository>();
            this.drinkmock = new Mock<IDrinkRepository>();
            this.testlogic = new SellerLogic(this.brandmock.Object, this.drinkmock.Object);
        }

        /// <summary>
        /// Test crate a brand.
        /// </summary>
        [Test]
        public void TestCreateBrand()
        {
            Brand b1 = new Brand();
            this.brandmock.Setup(x => x.Insert(It.IsAny<Brand>()));
            this.testlogic.CreateBrand(b1);
            this.brandmock.Verify(x => x.Insert(b1), Times.Once);
        }

        /// <summary>
        /// Teset get all drink.
        /// </summary>
        [Test]
        public void TestDrinkGetAll()
        {
            List<DrinksT> drinks = new List<DrinksT>()
            {
             new DrinksT() { DrinkID = 1, DrinkName = "Hendricks", Type = "Gin", BrandID = 1, Alcpercent = 41, Price = 9990 },
             new DrinksT() { DrinkID = 2, DrinkName = "Hendricks Midsummer", Type = "Gin", BrandID = 1, Alcpercent = 44.3, Price = 14990 },
            };

            List<DrinksT> expected = new List<DrinksT>() { drinks[1] };
            this.drinkmock.Setup(x => x.GetAll()).Returns(expected.AsQueryable());
            var drink = this.testlogic.GetAllDrinks();
            this.brandmock.Verify(x => x.GetAll(), Times.Never);
        }

        /// <summary>
        /// Test - strong drink.
        /// </summary>
        [Test]
        public void TestStrongDrinks()
        {
            Brand b1 = new Brand() { BrandID = 1, BrandName = "Hendricks" };
            Brand b2 = new Brand() { BrandID = 2, BrandName = "Öreg" };
            DrinksT d1 = new DrinksT() { DrinkID = 1, Brand = b1, DrinkName = "Hendricks", Type = "Gin", BrandID = 1, Alcpercent = 41, Price = 9990 };
            DrinksT d2 = new DrinksT() { DrinkID = 2, Brand = b1, DrinkName = "Hendricks Midsummer", Type = "Gin", BrandID = 1, Alcpercent = 22, Price = 14990 };
            DrinksT d3 = new DrinksT() { DrinkID = 3, Brand = b2, DrinkName = "Öreg", Type = "Gin", BrandID = 2, Alcpercent = 40, Price = 19990 };
            List<DrinksT> drinks = new List<DrinksT>()
            {
                d1,
                d2,
                d3,
            };

            List<Brand> brands = new List<Brand>() { b1, b2 };

            List<StrongDrink> expected = new List<StrongDrink>
            {
                new StrongDrink() { DrinkName = "Hendricks", BrandName = "Hendricks" },
                new StrongDrink() { DrinkName = "Öreg", BrandName = "Öreg" },
            };

            this.brandmock.Setup(x => x.GetAll()).Returns(brands.AsQueryable());
            this.drinkmock.Setup(x => x.GetAll()).Returns(drinks.AsQueryable());
            IList<StrongDrink> asd = this.testlogic.StrongDrinks(30);
            Assert.That(asd, Is.EquivalentTo(expected));
        }

        /// <summary>
        /// Test most expensive drink.
        /// </summary>
        [Test]
        public void TestMostExpensiveDrink()
        {
            DrinksT d1 = new DrinksT() { DrinkID = 1, DrinkName = "Hendricks", Type = "Gin", BrandID = 1, Alcpercent = 41, Price = 9990 };
            DrinksT d2 = new DrinksT() { DrinkID = 2, DrinkName = "Hendricks Midsummer", Type = "Gin", BrandID = 1, Alcpercent = 44.3, Price = 14990 };
            DrinksT d3 = new DrinksT() { DrinkID = 3, DrinkName = "Öreg", Type = "Gin", BrandID = 2, Alcpercent = 40, Price = 19990 };
            List<DrinksT> drinks = new List<DrinksT>() { d1, d2, d3 };
            this.drinkmock.Setup(x => x.GetAll()).Returns(drinks.AsQueryable());
            var result = this.testlogic.MostExpensiveDrink();
            Assert.That(d3, Is.EqualTo(result));
        }

        /// <summary>
        /// Test - Brand Avrages.
        /// </summary>
        [Test]
        public void TestBrandAvragesResult()
        {
            Brand b2 = new Brand() { BrandID = 2, BrandName = "Öreg" };
            Brand b1 = new Brand() { BrandID = 1, BrandName = "Hendricks" };
            List<Brand> brands = new List<Brand> { b1, b2, };
            List<DrinksT> drinks = new List<DrinksT>()
            {
               new DrinksT() { BrandID = 1, DrinkName = "Hendricks", Price = 20000, Brand = b2 },
               new DrinksT() { BrandID = 1, DrinkName = "Hendricks Midsummer", Price = 10000, Brand = b2 },
               new DrinksT() { BrandID = 2, DrinkName = "Öreg", Price = 25000, Brand = b1 },
               new DrinksT() { BrandID = 2, DrinkName = "Jameson", Price = 75000, Brand = b1 },
            };
            List<AverageResult> expected = new List<AverageResult>()
            {
               new AverageResult() { Avg = 50000, BrandID = 1, BrandName = "Hendricks" },
               new AverageResult() { Avg = 15000, BrandID = 2, BrandName = "Öreg" },
            };

            this.drinkmock.Setup(x => x.GetAll()).Returns(drinks.AsQueryable());
            this.brandmock.Setup(x => x.GetAll()).Returns(brands.AsQueryable());
            IList<AverageResult> res = this.testlogic.GetPriveAvrage();
            Assert.That(res, Is.EquivalentTo(expected));
            this.drinkmock.Verify(x => x.GetAll(), Times.Once);
            this.brandmock.Verify(x => x.GetAll(), Times.Never);
        }
    }
}
