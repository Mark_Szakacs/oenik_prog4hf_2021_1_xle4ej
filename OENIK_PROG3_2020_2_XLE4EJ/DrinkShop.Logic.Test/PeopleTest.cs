﻿// <copyright file="PeopleTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrinkShop.Logic.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DrinkShop.Data.Drinks;
    using DrinkShop.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// People test class.
    /// </summary>
    [TestFixture]
    public class PeopleTest
    {
        private Mock<ICustomerRepository> customermock;
        private Mock<IPurchaseRepository> purchasemock;
        private PeopleLogic testlogic;

        /// <summary>
        /// Setup test.
        /// </summary>
        [SetUp]
        public void SetupTest()
        {
            this.customermock = new Mock<ICustomerRepository>();
            this.purchasemock = new Mock<IPurchaseRepository>();
            this.testlogic = new PeopleLogic(this.customermock.Object, this.purchasemock.Object);
        }

        /// <summary>
        /// Test - update a customer's address.
        /// </summary>
        [Test]
        public void TestUpdateCustomerAddress()
        {
            Customer c1 = new Customer() { CostumerID = 1, CostumerName = "Eros Pista", Age = 19, Phone = "06301953097", CostumerAddress = "Árpád fejedelem útja 57" };
            this.customermock.Setup(x => x.UpdateAddress(c1.CostumerID ?? 0, c1.CostumerAddress)).Verifiable();
            this.testlogic.UpdateLocation(id: c1.CostumerID ?? 0, "Paks");
            this.customermock.Verify(x => x.UpdateAddress(c1.CostumerID ?? 0, "Paks"));
        }

        /// <summary>
        /// Test - deleting a purchase.
        /// </summary>
        [Test]
        public void TestDeletePurchase()
        {
            Purchase p2 = new Purchase();
            this.purchasemock.Setup(x => x.Delete((int)p2.PurchaseID)).Verifiable();
            this.testlogic.DeletePuch(p2.PurchaseID);
            this.purchasemock.Verify(x => x.Delete((int)p2.PurchaseID), Times.Never);
        }

        /// <summary>
        /// Test - Getone customer.
        /// </summary>
        [Test]
        public void TestGetOneCustomer()
        {
            var expectedResult = this.testlogic.GetCustomerById(2);

            this.customermock.Verify(x => x.GetOne(It.IsAny<int>()), Times.Once);
            this.customermock.Verify(x => x.GetAll(), Times.Never);
        }

        /// <summary>
        /// Test - Over Age.
        /// </summary>
        [Test]
        public void TestOverAge()
        {
            Purchase p1 = new Purchase() { PurchaseID = 1, CustomerName = " ", PurchaseDate = new DateTime(2008, 5, 14, 8, 30, 32) };
            Purchase p2 = new Purchase() { PurchaseID = 2, CustomerName = " ", PurchaseDate = new DateTime(1999, 12, 11, 9, 30, 52) };
            Purchase p3 = new Purchase() { PurchaseID = 3, CustomerName = " ", PurchaseDate = new DateTime(1994, 3, 23, 15, 30, 32) };
            List<Purchase> purchases = new List<Purchase> { p1, p2, p3 };
            Customer c1 = new Customer() { CostumerID = 1, CostumerName = "Eros Pista", Age = 19, Phone = "06301953097", CostumerAddress = "Árpád fejedelem útja 57", PurchaseID = 1, Purchase = p1 };
            Customer c2 = new Customer() { CostumerID = 2, CostumerName = "Mara A Müllner", Age = 39, Phone = "06301953097", CostumerAddress = "Nyár utca 16", PurchaseID = 1, Purchase = p2 };
            List<Customer> customers = new List<Customer>() { c1, c2 };
            List<OverAge> expected = new List<OverAge>()
            {
                new OverAge() { CustomerName = "Eros Pista", Age = 19, PurchaseTime = new DateTime(2008, 5, 14, 8, 30, 32) },
                new OverAge() { CustomerName = "Mara A Müllner", Age = 39, PurchaseTime = new DateTime(1999, 12, 11, 9, 30, 52) },
            };

            this.customermock.Setup(x => x.GetAll()).Returns(customers.AsQueryable());
            this.purchasemock.Setup(x => x.GetAll()).Returns(purchases.AsQueryable());
            IList<OverAge> res = this.testlogic.OverAge();
            Assert.That(res, Is.EquivalentTo(expected));
            this.customermock.Verify(x => x.GetAll(), Times.Once);
            this.purchasemock.Verify(x => x.GetAll(), Times.Never);
        }
    }
}
